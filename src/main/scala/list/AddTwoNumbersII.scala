package list

object AddTwoNumbersII {
  // Runtime: 388 ms, faster than 37.50% of Scala online submissions for Add Two Numbers II.
  def addTwoNumbers(l1: ListNode, l2: ListNode): ListNode = {

    convertToLeetCodeList(addTwoNumbers(convertToScalaList(l1).reverse, convertToScalaList(l2).reverse, 0).reverse)
  }

  def addTwoNumbers(l1: List[Int], l2: List[Int], carry: Int) : List[Int] = {
    var h = 0
    l1 match {
      case x::_ => l2 match {
        case y::_ =>
          h = carry + x + y
          (h % 10)::addTwoNumbers(l1.tail, l2.tail, h/10)
        case Nil =>
          h = carry + x
          (h % 10)::addTwoNumbers(l1.tail, Nil, h/10)
      }
      case Nil => l2 match {
        case y::_ =>
          h = carry + y
          (h % 10)::addTwoNumbers(Nil, l2.tail, h/10)
        case Nil =>
          if (carry != 0) carry::addTwoNumbers(Nil, Nil, 0)
          else Nil
      }
    }
  }

  def convertToScalaList(l: ListNode): List[Int] = {
    if (l == null) Nil
    else l.x::convertToScalaList(l.next)
  }

  def convertToLeetCodeList(ll: List[Int]): ListNode = ll match {
    case h::_ =>
      val head = new ListNode(h)
      head.next = convertToLeetCodeList(ll.tail)
      head
    case Nil => null
  }
}
