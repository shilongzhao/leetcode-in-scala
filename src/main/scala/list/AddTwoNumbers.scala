package list

object AddTwoNumbers {
  def addTwoNumbers(l1: ListNode, l2: ListNode): ListNode = {
    val head = new ListNode
    var carry = 0
    var list1 = l1
    var list2 = l2

    var p = head
    while (list1 != null || list2 != null || carry != 0) {
      val n = new ListNode
      var v = carry
      if (list1 != null) {v += list1.x; list1 = list1.next}
      if (list2 != null) {v += list2.x; list2 = list2.next}
      n.x = v % 10
      carry = v / 10
      p.next = n; p = n
    }
    head.next
  }


}
