package list

/**
  * https://leetcode.com/problems/split-linked-list-in-parts/
  */
object SplitLinkedListInPairs {
  def splitListToParts(root: ListNode, k: Int): Array[ListNode] = {
    var arr =  Array[ListNode]()
    var p = root
    while(p != null) {
      arr = arr :+ p
      p = p.next
    }
    val len = arr.length / k;
    var rem = arr.length % k;

    var res:Array[ListNode] = Array()

    var index = 0
    (0 until k).foreach(i => {
      val add = if (rem > 0) {rem -= 1; 1} else 0

      if (index < arr.length && index > 0)
        arr(index - 1).next = null

      res = res :+ (if (index >= arr.length) null else arr(index))

      index = index + (len + add)

    })
    res
  }

  def main(args: Array[String]): Unit = {
    val l1 = new ListNode(1)
    val l2 = new ListNode(2)
    val l3 = new ListNode(3)
    val l4 = new ListNode(4)
    l1.next = l2
    l2.next = l3
    l3.next = l4
    l4.next = null
    splitListToParts(l1, 5)
  }
}
