package list

object AddTwoNumbersScalaList {

  def addTwoNumbers(l1: List[Int], l2: List[Int]) : List[Int] = {
    addLittleEndian(l1.reverse, l2.reverse, 0).reverse
  }

  def addLittleEndian(l1: List[Int], l2: List[Int], carry: Int) : List[Int] = {
    var h = 0
    l1 match {
      case x::_ => l2 match {
        case y::_ =>
          h = carry + x + y
          (h % 10)::addLittleEndian(l1.tail, l2.tail, h/10)
        case Nil =>
          h = carry + x
          (h % 10)::addLittleEndian(l1.tail, Nil, h/10)
      }
      case Nil => l2 match {
        case y::_ =>
          h = carry + y
          (h % 10)::addLittleEndian(Nil, l2.tail, h/10)
        case Nil =>
          if (carry != 0) carry::addLittleEndian(Nil, Nil, 0)
          else Nil
      }
    }
  }

  def main(args: Array[String]): Unit = {
    val a = List(1,0,0,0,0,0,0,0,0,2,3)
    val b = List(2,3,4,5,8,9)

    println(addTwoNumbers(a, b))
  }
}
