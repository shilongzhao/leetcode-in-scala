package list;


import java.util.ArrayList;
import java.util.List;

// Definition for a Node.
class Node {
    public int val;
    public Node prev;
    public Node next;
    public Node child;

    public Node() {}

    public Node(int _val,Node _prev,Node _next,Node _child) {
        val = _val;
        prev = _prev;
        next = _next;
        child = _child;
    }
};

/**
 * https://leetcode.com/problems/flatten-a-multilevel-doubly-linked-list
 * Runtime: 0 ms, faster than 100.00% of Java online submissions for Flatten a Multilevel Doubly Linked List.
 */
public class FlattenDoublyLinkedList {
    public Node flatten(Node head) {
        Node p = head;
        while (p != null) {
            // Do not change the order of the following 2 lines
            Node t = p.next;
            Node tail = flattenWithTail(p);
            tail.next = t;
            if (t != null) {
                t.prev = tail;
            }
            p = t;
        }
        return head;
    }

    /**
     * flatten a node, and return the flattened list's tail
     * @param head
     * @return
     */
    public Node flattenWithTail(Node head) {
        if (head.child == null) return head;

        Node c = head.child;
        head.next = c;
        c.prev = head;

        head.child = null;
        Node prev = head;
        while (c != null) {

            // Do not change the order of the following 2 lines
            Node t = c.next;
            Node tail = flattenWithTail(c);

            tail.next = t;
            if (t != null) {
                t.prev = tail;
            }

            prev = tail;
            c = t;
        }

        return prev;
    }
}
