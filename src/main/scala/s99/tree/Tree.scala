package s99.tree

sealed abstract class Tree[+T] {
  /**
    * P57 add value in binary trees
    * @param x new value
    * @param ordered ordered implicit parameter
    * @tparam U super type of T
    * @return a new tree
    */
  def addValue[U >: T](x: U)(implicit ordered: U => Ordered[U]): Tree[U]
}

case class Node[+T](value: T, left: Tree[T], right: Tree[T]) extends Tree[T] {
  override def toString: String = "T(" + value.toString + " " + left.toString + " " + right.toString + ")"

  override def addValue[U >: T](x: U)(implicit ordered: U => Ordered[U]): Tree[U] = {
    if (x == value) Tree(value, left, right)
    else if (x < value) Tree(value, left.addValue(x), right)
    else Tree(value, left, right.addValue(x))
  }
}

case object End extends Tree[Nothing] {
  override def toString: String = "."
  override def addValue[U >: Nothing](x: U)(implicit ordered: U => Ordered[U]): Tree[U] = Tree(x, End, End)
}

object Tree {
  def apply[T](): Tree[T] = End
  def apply[T](value: T): Tree[T] = Node(value, End, End)
  def apply[T](value: T, left: Tree[T], right: Tree[T]): Tree[T] = Node(value, left, right)
  /**
    * Generate a BST from a list
    *
    * @param elements list
    * @param ordered order
    * @tparam T type param
    * @return
    */
  def fromList[T](elements: List[T])(implicit ordered: T => Ordered[T]): Tree[T] =
    elements.foldLeft(Tree[T]())((t, e) => t.addValue(e))

}
