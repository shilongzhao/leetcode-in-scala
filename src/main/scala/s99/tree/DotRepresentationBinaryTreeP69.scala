package s99.tree

import scala.collection.mutable

/**
  * P69 (**) Dotstring representation of binary trees.
  * We consider again binary trees with nodes that are identified by single lower-case letters, as in the example of problem P67. Such a tree can be represented by the preorder sequence of its nodes in which dots (.) are inserted where an empty subtree (End) is encountered during the tree traversal. For example, the tree shown in problem P67 is represented as "abd..e..c.fg...". First, try to establish a syntax (BNF or syntax diagrams) and then write two methods, toDotstring and fromDotstring, which do the conversion in both directions.
  * scala> Tree.string2Tree("a(b(d,e),c(,f(g,)))").toDotstring
  * res0: String = abd..e..c.fg...
  *
  * scala> Tree.fromDotstring("abd..e..c.fg...")
  * res1: Node[Char] = a(b(d,e),c(,f(g,)))
  * The file containing the full class definitions for this section is tree.scala.
  */
object DotRepresentationBinaryTreeP69 {

  def toDotString(tree: Tree[Char]): String = tree match {
    case End => "."
    case Node(value, left, right) => (value +: toDotString(left)) ++ toDotString(right)
  }

  def fromDotString(input: String): Tree[Char] = {
    var rev = input.reverse
    val stack = mutable.Stack[Tree[Char]]()
    while (rev.nonEmpty) {
      if (rev.head == '.') stack.push(End)
      else {
        val l = stack.pop
        val r = stack.pop
        stack.push(Tree(rev.head, l, r))
      }
      rev = rev.tail
    }
    stack.pop
  }

  def main(args: Array[String]): Unit = {
    import StringToBinaryTreeConvertP67._
    val stringRep = "a(b(d,e),c(,f(g,)))"
    val targetDotRep = "abd..e..c.fg..."
    val dotRep = toDotString(stringToBinaryTree(stringRep))
    println(s"$dotRep should be the same as $targetDotRep? ${dotRep == targetDotRep}")

    val tree = fromDotString(targetDotRep)
    import BinaryTreeStringConvertP67._
    println(s"generated tree ${tree.stringRep} should be $stringRep? ${stringRep == tree.stringRep}")
  }
}
