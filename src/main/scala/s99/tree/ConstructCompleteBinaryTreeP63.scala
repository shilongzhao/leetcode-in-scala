package s99.tree

object ConstructCompleteBinaryTreeP63 {
  def completeBinaryTree[T](n: Int, value: T): Tree[T] = ???

  def treeLevels(numNodes: Int): Int = Math.ceil(Math.log(numNodes + 1)/Math.log(2)).toInt

  def main(args: Array[String]): Unit = {
    println(treeLevels(1)) // 1
    println(treeLevels(3)) // 2
    println(treeLevels(4)) // 3
    println(treeLevels(5)) // 3
    println(treeLevels(6)) // 3
    println(treeLevels(7)) // 3
    println(treeLevels(8)) // 4
  }
}
