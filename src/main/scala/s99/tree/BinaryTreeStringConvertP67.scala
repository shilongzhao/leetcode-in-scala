package s99.tree

import scala.collection.mutable

object BinaryTreeStringConvertP67 {

  implicit def treeToTreeStringConverter[T](tree: Tree[T]): TreeStringConverter[T] = new TreeStringConverter[T](tree)

  class TreeStringConverter[T](tree: Tree[T]) {
    def stringRep: String = tree match {
      case End => ""
      case Node(value, End, End) => value.toString
      case Node(value, left, right) => value.toString + "(" + left.stringRep + "," + right.stringRep + ")"
    }
  }

}
