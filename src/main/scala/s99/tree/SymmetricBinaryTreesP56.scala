package s99.tree

/**
  * P56 (**) Symmetric binary trees.
  *
  * Let us call a binary tree symmetric if you can draw a vertical line through the root node and
  * then the right subtree is the mirror image of the left subtree. Add an isSymmetric method to the Tree class to
  * check whether a given binary tree is symmetric. Hint: Write an isMirrorOf method first to check whether one tree
  * is the mirror image of another. We are only interested in the structure, not in the contents of the nodes.
  *
  * scala> Node('a', Node('b'), Node('c')).isSymmetric
  * res0: Boolean = true
  */
object SymmetricBinaryTreesP56 {

  implicit def treeToSymmetricChecker[T](root: Tree[T]): SymmetricChecker[T] = SymmetricChecker(root)

  case class SymmetricChecker[T](root: Tree[T]) {
    def isSymmetric: Boolean = root match {
      case Node(_, l, r) => isMirror(l, r)
      case End => true
    }

    def isMirror(t1: Tree[T], t2: Tree[T]): Boolean = t1 match {
      case End => t2 match {
        case End => true
        case _ => false
      }
      case Node(_, l1, r1) => t2 match {
        case Node(_, l2, r2) => isMirror(l1, r2) && isMirror(r1, l2)
        case _ => false
      }
    }
  }
}