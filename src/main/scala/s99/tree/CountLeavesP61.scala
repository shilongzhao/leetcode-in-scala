package s99.tree

/**
  * P61 (*) Count the leaves of a binary tree.
  * A leaf is a node with no successors. Write a method leafCount to count them.
  * scala> Node('x', Node('x'), End).leafCount
  * res0: Int = 1
  */
object CountLeavesP61 {
  implicit def treeToLeavesCounter[T](tree: Tree[T]): LeavesCounter[T] = new LeavesCounter[T](tree)

  class LeavesCounter[T](tree: Tree[T]) {

    def leafCount: Int = tree match {
      case End => 0
      case Node(_, End, End) =>  1
      case Node(_, left, right) => left.leafCount + right.leafCount
    }
  }
}
