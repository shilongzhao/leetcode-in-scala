package s99.tree

/**
  * _____  _____  _____  _____  _____ 
  * |__   ||  |  ||  _  ||     ||   __|
  * |   __||     ||     ||  |  ||__   |
  * |_____||__|__||__|__||_____||_____|
  *
  * P62 (*) Collect the internal nodes of a binary tree in a list.
  * An internal node of a binary tree has either one or two non-empty successors.
  * Write a method internalList to collect them in a list.
  *
  * scala> Node('a', Node('b'), Node('c', Node('d'), Node('e'))).internalList
  * res0: List[Char] = List(a, c)
  *
  */
object InternalNodesTreeP62 {

  implicit def treeToInternalListCounter[T](tree: Tree[T]): InternalListCounter[T] = new InternalListCounter[T](tree)

  class InternalListCounter[T](tree: Tree[T]) {
    def internalList: List[T] = tree match {
      case End => List()
      case Node(_, End, End) => List()
      case Node(value, left, right) => value::(left.internalList:::right.internalList)
    }
  }
}
