package s99.tree

/**
  * _____  _____  _____  _____  _____ 
  * |__   ||  |  ||  _  ||     ||   __|
  * |   __||     ||     ||  |  ||__   |
  * |_____||__|__||__|__||_____||_____|
  * P62B (*) Collect the nodes at a given level in a list.
  * A node of a binary tree is at level N if the path from the root to the node has length N-1.
  * The root node is at level 1. Write a method atLevel to collect all nodes at a given level in a list.
  *
  * scala> Node('a', Node('b'), Node('c', Node('d'), Node('e'))).atLevel(2)
  * res0: List[Char] = List(b, c)
  *
  * Using atLevel it is easy to construct a method levelOrder which creates the level-order sequence of the nodes.
  * However, there are more efficient ways to do that.
  *
  */
object NodesAtLevelP62B {

  implicit def treeToLevelCollector[T](tree: Tree[T]): TreeNodeLevelCollector[T] = new TreeNodeLevelCollector[T](tree)

  class TreeNodeLevelCollector[T](tree: Tree[T]) {
    def atLevel(level: Int): List[T] = {

      def treeNodeLevel(node: Tree[T], currentLevel: Int): List[T] = node match {
        case End => List()
        case Node(value, left, right) =>
          if (currentLevel == level) List(value)
          else treeNodeLevel(left, currentLevel + 1):::treeNodeLevel(right, currentLevel + 1)
      }

      treeNodeLevel(tree, 1)
    }
  }
}
