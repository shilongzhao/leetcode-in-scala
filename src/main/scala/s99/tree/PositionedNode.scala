package s99.tree

abstract class PositionedTree[+T]

case class PositionedNode[+T](value: T, left: PositionedTree[T], right: PositionedTree[T],val x: Int, val y: Int) extends PositionedTree[T] {
  override def toString: String =
    "T[" + x.toString + "," + y.toString + "](" + value.toString + ")"
}

case object PositionedEnd extends PositionedTree[Nothing] {
  override def toString: String =
    "E"
}
