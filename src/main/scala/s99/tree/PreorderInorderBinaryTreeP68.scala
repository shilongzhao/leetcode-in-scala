package s99.tree

/**
  * P68 (**) Preorder and inorder sequences of binary trees.
  * We consider binary trees with nodes that are identified by single lower-case letters, as in the example of problem P67.
  * a) Write methods preorder and inorder that construct the preorder and inorder sequence of a given binary tree, respectively. The results should be lists, e.g. List('a','b','d','e','c','f','g') for the preorder sequence of the example in problem P67.
  *
  * scala> Tree.string2Tree("a(b(d,e),c(,f(g,)))").preorder
  * res0: List[Char] = List(a, b, d, e, c, f, g)
  *
  * scala> Tree.string2Tree("a(b(d,e),c(,f(g,)))").inorder
  * res1: List[Char] = List(d, b, e, a, c, g, f)
  * b) If both the preorder sequence and the inorder sequence of the nodes of a binary tree are given, then the tree is determined unambiguously. Write a method preInTree that does the job.
  *
  * scala> Tree.preInTree(List('a', 'b', 'd', 'e', 'c', 'f', 'g'), List('d', 'b', 'e', 'a', 'c', 'g', 'f'))
  * res2: Node[Char] = a(b(d,e),c(,f(g,)))
  * What happens if the same character appears in more than one node? Try, for instance, Tree.preInTree(List('a', 'b', 'a'), List('b', 'a', 'a')).
  */
object PreorderInorderBinaryTreeP68 {

  implicit  def getOrderTraverser[T](tree: Tree[T]): OrderTraverser[T] = new OrderTraverser[T](tree)

  class OrderTraverser[T](tree: Tree[T]) {
    def preorder: Seq[T] = tree match {
      case End => Seq()
      case Node(v, l, r) => v +: (l.preorder ++ r.preorder)
    }

    def inorder: Seq[T] = tree match {
      case End => Seq()
      case Node(v, l, r) => (l.inorder :+ v) ++ r.inorder
    }


  }

  def preInTree[T](preorder: Seq[T], inorder: Seq[T]): Tree[T] = {
    if (preorder.isEmpty) {
      End
    } else {
      val root = preorder.head
      val rootIndex = inorder.indexOf(root)

      val leftInorder = inorder.slice(0, rootIndex)
      val rightInorder = inorder.slice(rootIndex + 1, inorder.size)

      val leftPreorder = preorder.slice(1, 1 + leftInorder.size)
      val rightPreorder = preorder.slice(1 + leftInorder.size, preorder.size)

      Tree(root, preInTree(leftPreorder, leftInorder), preInTree(rightPreorder, rightInorder))
    }
  }

  def main(args: Array[String]): Unit = {
    import StringToBinaryTreeConvertP67._
    val preorder = stringToBinaryTree("a(b(d,e),c(,f(g,)))").preorder
    println(s"preorder of tree a(b(d,e),c(,f(g,))) should be (a,b,c,d,e,f,g)? $preorder")

    val inorder = stringToBinaryTree("a(b(d,e),c(,f(g,)))").inorder
    println(s"inorder of tree a(b(d,e),c(,f(g,))) should be (d, b, e, a, c, g, f)? $inorder")

    val pre = Seq('a', 'b', 'd', 'e', 'c', 'f', 'g')
    val in = Seq('d', 'b', 'e', 'a', 'c', 'g', 'f')

    val value = preInTree(pre, in)
    println(s"generated trees preorder should be the same as $pre? ${value.preorder == pre}")
    println(s"generated trees inorder should be the same as $in? ${value.inorder == in}")
  }
}
