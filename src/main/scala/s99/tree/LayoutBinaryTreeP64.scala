package s99.tree

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
  * As a preparation for drawing a tree, a layout algorithm is required to determine the position of each node in a rectangular grid.
  * Several layout methods are conceivable, one of them is shown in the illustration on the right.
  * In this layout strategy, the position of a node v is obtained by the following two rules:
  *
  * x(v) is equal to the position of the node v in the inorder sequence
  * y(v) is equal to the depth of the node v in the tree
  */
object LayoutBinaryTreeP64 {

  /**
    * a vanilla method, that returns a triple
    * @param root tree root
    * @param seq sequence
    * @param depth depth
    * @tparam T value type of root
    * @return a triple with node value, the node's sequence in the inorder traversal, and the depth of the node
    */
  def inorder[T](root: Tree[T], seq: Int, depth: Int): mutable.ListBuffer[(T, Int, Int)] = root match {
    case End => ListBuffer()
    case Node(value, left, right) =>
      val l = inorder(left, seq, depth + 1)
      l.append((value, seq + l.size, depth))
      val r = inorder(right, seq + l.size, depth + 1)
      l.appendAll(r)
      l
  }

  /**
    * Better/Nicer solution?
    *
    * @param root root of the binary tree
    * @param seq the existing max seq until now in inorder traversal (it should corresponds to the param passed when calling)
    *            if you define this seq as the next possible seq can be used, then in the main function, seq should start from 1 instead of 0
    * @param depth depth in the tree
    * @tparam T value type in the root
    * @return a tuple, containing the root of the newly generated tree (with positions) and also the next sequence number
    */
  def inorderPositioned[T](root: Tree[T], seq: Int, depth: Int): (PositionedTree[T], Int) = root match {
    case End => (PositionedEnd, seq)
    case Node(value, left, right) => {
      val pl = inorderPositioned(left, seq, depth + 1)
      val pr = inorderPositioned(right, pl._2 + 1, depth + 1); // last max seq (root's seq is pl._2 + 1)
      (PositionedNode(value, pl._1, pr._1, pl._2 + 1, depth), pr._2) // the root node's seq is pl._2 + 1
    }
  }

  def printPositionedTree[T](positionedTree: PositionedTree[T]): Unit = positionedTree match {
    case PositionedEnd =>
    case PositionedNode(value, left, right, x, y) => {
      printPositionedTree(left)
      println(s"T[$x, $y]($value)")
      printPositionedTree(right)
    }
  }

  def main(args: Array[String]): Unit = {
//    inorder(Tree.fromList(List(4,6,2,3,5,9,8, 1,7)), 0, 0).foreach(print);
    printPositionedTree(inorderPositioned(Tree.fromList(List(4,6,2,3,5,9,8, 1,7)), 0, 0)._1)
    println("===============")
    printPositionedTree(inorderPositioned(Tree.fromList(List(6,1,2,5,3,8,0,7,9,10,15,12,17,13)), 0, 0)._1)
  }
}
