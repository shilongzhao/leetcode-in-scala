package s99.tree

/**
  * P58 (**) Generate-and-test paradigm.
  * Apply the generate-and-test paradigm to construct all symmetric,
  * completely balanced binary trees with a given number of nodes.
  *
  * scala> Tree.symmetricBalancedTrees(5, "x")
  * res0: List[Node[String]] = List(T(x T(x . T(x . .)) T(x T(x . .) .)), T(x T(x T(x . .) .) T(x . T(x . .))))
  */
object SymmetricBalancedTreesP58 {
  def symmetricBalancedTrees[T](n: Int, value: T):List[Tree[T]] = ???
}
