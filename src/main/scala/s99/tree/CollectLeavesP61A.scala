package s99.tree

object CollectLeavesP61A {

  implicit def treeToLeavesCollector[T](tree: Tree[T]): LeavesCollector[T] = new LeavesCollector[T](tree)

  class LeavesCollector[T](tree: Tree[T]) {
    def leafList: List[Node[T]] = tree match {
      case End => List()
      case Node(value, End, End) => List(Node(value, End, End))
      case Node(_, left, right) => left.leafList:::right.leafList
    }
  }
}
