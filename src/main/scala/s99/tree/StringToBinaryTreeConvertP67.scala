package s99.tree

/**
  * P67 (**) A string representation of binary trees.
  * Somebody represents binary trees as strings of the following type (see example opposite):
  * a(b(d,e),c(,f(g,)))
  * Write a method which generates this string representation, if the tree is given as usual (in Nodes and Ends).
  * Use that method for the Tree class's and subclass's toString methods.
  *
  * Then write a method (on the Tree object) which does this inverse; i.e. given the string representation,
  * construct the tree in the usual form.
  *
  * For simplicity, suppose the information in the nodes is a single letter and there are no spaces in the string.
  *
  * scala> Node('a', Node('b', Node('d'), Node('e')), Node('c', End, Node('f', Node('g'), End))).toString
  * res0: String = a(b(d,e),c(,f(g,)))
  *
  * scala> Tree.fromString("a(b(d,e),c(,f(g,)))")
  * res1: Node[Char] = a(b(d,e),c(,f(g,)))
  */
object StringToBinaryTreeConvertP67 {

  def stringToBinaryTree(seq: Seq[Char]): Tree[Char] = {
    if (seq.isEmpty) End
    else {
      // take the first char in k(.., ..), k,
      val key = seq.head
      if (seq.tail.isEmpty) {
        Tree(key)
      }
      else {
        val (left, right) = split(seq.tail)
        Tree(key, stringToBinaryTree(left), stringToBinaryTree(right))
      }
    }
  }

  /**
    * (abc, def)
    *
    * @param chars input
    * @return
    */
  def split(chars: Seq[Char]): (Seq[Char], Seq[Char]) = {
    var count = 0
    var mid = 0
    for (i <- 1 until chars.size - 1) { // exclude starting and ending parenthesis
      if (chars(i) == '(') count = count + 1
      else if (chars(i) == ')') {
        count = count - 1
      } else if (chars(i) == ',') {
        if (count == 0) mid = i
      }
    }
    (chars.slice(1, mid), chars.slice(mid + 1, chars.size - 1))
  }

  def main(args: Array[String]): Unit = {

    import BinaryTreeStringConvertP67._
    var value = "a(,b(,c(,d(e,f))))"
    var root = stringToBinaryTree(value)
    println(s"1. string representation equals input $value? ${root.stringRep == value}")

    value = "a(b(c(d(e,f),),),)"
    root = stringToBinaryTree(value)
    println(s"2. string representation equals input $value? ${root.stringRep == value}")

    value = "a(b(d,e),c(,f(g,)))"
    root = stringToBinaryTree(value)
    println(s"3. string representation equals input $value? ${root.stringRep == value}")

  }

}
