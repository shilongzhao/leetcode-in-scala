package s99.logic

import scala.collection.mutable

/**
  * P50 (***) Huffman code.
  * First of all, consult a good book on discrete mathematics or algorithms for a detailed description of Huffman codes!
  * We suppose a set of symbols with their frequencies, given as a list of (S, F) Tuples. E.g. (("a", 45), ("b", 13), ("c", 12), ("d", 16), ("e", 9), ("f", 5)). Our objective is to construct a list of (S, C) Tuples, where C is the Huffman code word for the symbol S.
  *
  * scala> huffman(List(("a", 45), ("b", 13), ("c", 12), ("d", 16), ("e", 9), ("f", 5)))
  * res0: List[String, String] = List((a,0), (b,101), (c,100), (d,111), (e,1101), (f,1100))
  */
object HuffmanCode {
  def huffman(input: List[(Char, Int)]) : mutable.Map[Char, String] = {
    val root = buildTrie(input)
    val map = mutable.Map[Char, String]().withDefaultValue("")
    def buildCode(trie: Node, s: String):Unit = {
      if (trie.isLeaf) map.put(trie.c, s)
      if (trie.left != null) buildCode(trie.left, s + "0")
      if (trie.right != null) buildCode(trie.right, s + "1")
    }
    buildCode(root, "")
    map
  }


  case class Node(c: Char, freq: Int, left: Node, right: Node) {
    def isLeaf: Boolean = left == null && right == null
  }

  object NodeOrdering extends Ordering[Node] {
    override def compare(x: Node, y: Node): Int = y.freq compare x.freq
  }

  def buildTrie(input: List[(Char, Int)]): Node = {
    // min heap
    val pq = mutable.PriorityQueue()(NodeOrdering)
    input.foreach(t => pq.enqueue(Node(t._1, t._2, null, null)))
    while(pq.size > 1) {
      val left = pq.dequeue()
      val right = pq.dequeue()
      val parent = Node('\0', left.freq + right.freq, left, right)
      pq.enqueue(parent)
    }
    pq.dequeue()
  }

  def main(args: Array[String]): Unit = {
    val input = List(('A', 12), ('B', 23), ('C', 8), ('D', 17))

    println(huffman(input))

    val input2 = List(('a', 45), ('b', 13), ('c', 12), ('d', 16), ('e', 9), ('f', 5))

    println(huffman(input2))
  }
}
