package s99.logic

/**
  * P49 (**) Gray code.
  * An n-bit Gray code is a sequence of n-bit strings constructed according to certain rules. For example,
  * n = 1: C(1) = ("0", "1").
  * n = 2: C(2) = ("00", "01", "11", "10").
  * n = 3: C(3) = ("000", "001", "011", "010", "110", "111", "101", "100").
  * Find out the construction rules and write a function to generate Gray codes.
  *
  * scala> gray(3)
  * res0 List[String] = List(000, 001, 011, 010, 110, 111, 101, 100)
  * See if you can use memoization to make the function more efficient.
  *
  * http://mathworld.wolfram.com/GrayCode.html
  */
object GreyCodeP49 {
  def gray(n: Int): List[String] = {
    if (n == 1) List("0", "1")
    else gray(n - 1).map("0" + _) ++ gray(n - 1).reverse.map("1" + _)
  }
}
