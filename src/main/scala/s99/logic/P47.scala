package s99.logic

/**
  * @author zsh
  *         created 2020/Sep/08
  */
object P47 {
  implicit class BooleanWrapper(a: Boolean) {
    def and(b: => Boolean): Boolean  = if (a) b else false
    def or(b: => Boolean): Boolean   = if (a) true else b
    def nand(b: => Boolean): Boolean = if (a) !b else true
    def nor(b: => Boolean): Boolean  = if (a) false else !b
    def xor(b: Boolean): Boolean     = a != b
    def impl(b: => Boolean): Boolean = if (a) b else true
    def equ(b: Boolean): Boolean     = a == b
  }
}
