package s99.multi_tree

object InternalPathLength {

  implicit def stringToMTreeInternalPath(str: String): MTreeInternalPath = new MTreeInternalPath(str)

  class MTreeInternalPath(caretString: String) {
    def internalPathLength: Int = {
      val tree = ConstructTreeFromStringP70.stringToMTree(caretString)
      internalPathLength(tree)
    }

    def internalPathLength[T](tree: MTree[T]):Int =
      tree.children.foldLeft(0)((b, t) => b + numNodes(t)) + tree.children.foldLeft(0)((b, t) => b + internalPathLength(t))
  }

  def numNodes[T](tree: MTree[T]): Int = 1 + tree.children.foldLeft(0)((b, t) => b + numNodes(t))

  def main(args: Array[String]): Unit = {
    val treeFunc = ConstructTreeFromStringP70.stringToMTree _
    val generatedTree = treeFunc("afg^^c^bd^e^^^")
    println(numNodes(generatedTree))
    println("afg^^c^bd^e^^^".internalPathLength)

    println("124^5^^36^7^^^".internalPathLength)

  }
}
