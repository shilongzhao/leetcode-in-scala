package s99.multi_tree

object PostOrderSeqP72 {

  implicit def caretStringToPostOrderSeq(caret: String): PostOrderSeq = new PostOrderSeq(caret)
  class PostOrderSeq(caretString: String) {
    def postOrder: List[Char] = {
      val root = ConstructTreeFromStringP70.stringToMTree(caretString)
      postOrder(root)
    }

    def postOrder[T](tree: MTree[T]): List[T] = {
      if (tree.children.isEmpty) List[T](tree.value)
      else tree.children.foldLeft(List[T]())((l, t) => l:::postOrder(t)) :+ tree.value
    }
  }

  def main(args: Array[String]): Unit = {
    println("afg^^c^bd^e^^^".postOrder) // List(g, f, c, d, e, b, a)
  }
}
