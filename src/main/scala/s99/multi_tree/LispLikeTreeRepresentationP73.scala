package s99.multi_tree

import scala.collection.mutable

object LispLikeTreeRepresentationP73 {

  implicit def toLispyTreePrinter[T](tree: MTree[T]): LispyTreePrinter[T] = new LispyTreePrinter[T](tree)

  class LispyTreePrinter[Char](tree: MTree[Char]) {
    def lispyTree: String = {
      if (tree.children.isEmpty) String.valueOf(tree.value)
      else "(" + tree.value + tree.children.foldLeft("")((s, t) => s + " " + t.lispyTree) + ")"
    }
  }

  class MTreeLispyStringConstructor(lispyString: String) {
    def tree: MTree[Char] = {
      var level = 0
      var map = mutable.Map[Int, List[MTree[Char]]]().withDefaultValue(List[MTree[Char]]())
      for (i <- 0 until lispyString.length) {
        if (lispyString(i) == '(') {
          level = level + 1
        }
        else if (lispyString(i) == ')') {
          val list: List[MTree[Char]] = map(level)
          val node: MTree[Char] = MTree(list.head.value, list.tail)
          map -= level

          val upper = map(level - 1)
          map += ((level - 1) -> (upper :+ node))

          level = level - 1
        }
        else if (lispyString(i) == ' ') {

        }
        else {
          val node: MTree[Char] = MTree(lispyString(i))
          val list: List[MTree[Char]] = map(level)
          map += (level -> (list :+ node))
        }
      }
      map(0).head
    }
  }

  def main(args: Array[String]): Unit = {
    val tree = MTree("a", List(MTree("b", List(MTree("c")))))
    println(tree.lispyTree)
    assert(tree.lispyTree == "(a (b c))", s"lispy representation should be (a (b c)) but instead is ${tree.lispyTree}")

    import ConstructTreeFromStringP70._
    val another = stringToMTree("afg^^c^bd^e^^^").lispyTree
    println(another)
    assert(another == "(a (f g) c (b d e))", s"lispy representation of tree afg^^c^bd^e^^^ should be (a (f g) c (b d e)) but instead is $another")

    val lispyToCaret = (new MTreeLispyStringConstructor("(a (f g) c (b d e))")).tree.toCaretString
    println(lispyToCaret)
    assert(lispyToCaret == "afg^^c^bd^e^^^", s"caret string representation of (a (f g) c (b d e)) should be equal to afg^^c^bd^e^^^, but is instead $lispyToCaret")
  }

}
