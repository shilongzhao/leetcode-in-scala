package s99.multi_tree

import scala.collection.mutable

/**
  * P70 (**) Tree construction from a node string.
  * We suppose that the nodes of a multiway tree contain single characters. In the depth-first order sequence of its nodes, a special character ^ has been inserted whenever, during the tree traversal, the move is a backtrack to the previous level.
  * By this rule, the tree in the figure opposite is represented as:
  *
  * afg^^c^bd^e^^^
  * Define the syntax of the string and write a function string2MTree to construct an MTree from a String. Make the function an implicit conversion from String. Write the reverse function, and make it the toString method of MTree.
  *
  * scala> MTree('a', List(MTree('f', List(MTree('g'))), MTree('c'), MTree('b', List(MTree('d'), MTree('e'))))).toString
  * res0: String = afg^^c^bd^e^^^
  **/
object ConstructTreeFromStringP70 {

  implicit def treeToCaretStringConverter[T](tree: MTree[T]): CaretStringConverter[T] = new CaretStringConverter[T](tree)
  class CaretStringConverter[T](tree: MTree[T]) {
    // DFS or preorder

    def toCaretString: String = tree.toCaretStringChildren + "^"

    def toCaretStringChildren: String = {
      if (tree.children.isEmpty)
        tree.value.toString
      else {
        tree.value.toString + tree.children.map(c => c.toCaretStringChildren + "^").mkString
      }
    }
  }

  def stringToMTree(input: String): MTree[Char] = {
    val rev:Seq[Char] = input.reverse
    var carets:Int = 0
    val map = mutable.Map[Int, List[MTree[Char]]]().withDefaultValue(List())
    rev.foreach {
      case '^' =>
        carets += 1
//        println(s"increasing carets = $carets")
      case c =>
//        println(s"carets = $carets char = $c")
        val node = if (map(carets + 1).nonEmpty) {
          val list = map(carets + 1)
          map -= (carets + 1)
          MTree(c, list)
        }
        else {
          MTree[Char](c)
        }
        val list = node::map(carets)
//        println(s"adding entry $carets -> list size = ${list.size} with $list")
        map += (carets -> list)
        carets -= 1
    }
//    println(s"map size = ${map.size}")
    map(1).head
  }

  def main(args: Array[String]): Unit = {
    val rep = MTree('a', List(MTree('f', List(MTree('g'))), MTree('c'), MTree('b', List(MTree('d'), MTree('e'))))).toCaretString
    println(rep)
    print(stringToMTree(rep).toCaretString == rep)
  }
}
