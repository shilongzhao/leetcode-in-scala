package s99.multi_tree

object CountNodesP70C {

  implicit def treeToNodeCounter[T](tree: MTree[T]): NodeCounter[T] = new NodeCounter[T](tree)

  class NodeCounter[T](tree: MTree[T]) {
    def nodeCount: Int =  {
      if (tree.children.isEmpty) 1
      else 1 +  tree.children.map(t => t.nodeCount).sum
    }
  }
}
