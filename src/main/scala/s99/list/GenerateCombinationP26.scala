package s99.list

object GenerateCombinationP26 {

  def combinations[T](n: Int, list: List[T]): List[List[T]] =
    combinations(List(), n, list)

  def combinations[T](partial: List[T], n: Int, list: List[T]): List[List[T]] = {
    if (n > list.size) List()
    else if (n == 0) List(partial)
    else  {
      combinations(partial :+ list.head, n - 1, list.tail) ++
      combinations(partial, n, list.tail)
    }
  }

  def combinationsV2[T](n: Int, list: List[T]): List[List[T]] = list.combinations(n).toList

  def combinationsV3[T](n: Int, list: List[T]): List[List[T]] = list match {
    case _ :: _ if n == 1 => list.map(List(_))
    case x::xs => combinationsV3(n - 1, xs).map(x::_) ::: combinationsV3(n, xs)
    case Nil => Nil
  }


  def combinations_v4[T](n: Int, list: List[T]) = ???
}
