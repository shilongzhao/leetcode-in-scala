package s99.list

object RandomNumbersInRangeP24 {
  /**
    * draw N different number in range [0, M)
    * @param N number of elements to draw
    * @param M max value
    * @return list
    */
  def lotto(N: Int, M: Int): List[Int] = {
    RandomSelectP23.randomSelectByRemove(N, (0 until M).toList)
  }
}
