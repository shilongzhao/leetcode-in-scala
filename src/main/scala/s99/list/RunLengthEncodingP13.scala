package s99.list

/**
  * P13 (**) Run-length encoding of a list (direct solution).
  * Implement the so-called run-length encoding data compression method directly. I.e. don't use other methods you've written (like P09's pack); do all the work directly.
  * Example:
  *
  * scala> encodeDirect(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
  * res0: List[(Int, Symbol)] = List((4,'a), (1,'b), (2,'c), (2,'a), (1,'d), (4,'e))
  */
object RunLengthEncodingP13 {
  def encodeDirect[T](list: List[T]): List[(Int, T)] = list match {
    case Nil => List()
    case x::xs => encode(List(), (1, x), xs)
  }

  @scala.annotation.tailrec
  def encode[T](result: List[(Int, T)], current: (Int, T), list: List[T]): List[(Int, T)] = list match {
    case Nil => result :+ current
    case x::xs =>
      if (x == current._2)
        encode(result, (current._1 + 1, x), xs)
      else
        encode(result :+ current, (1, x), xs)
  }
}
