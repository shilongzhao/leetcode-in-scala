package s99.list

/**
  * P12 (**) Decode a run-length encoded list.
  * Given a run-length code list generated as specified in problem P10, construct its uncompressed version.
  * Example:
  *
  * scala> decode(List((4, 'a), (1, 'b), (2, 'c), (2, 'a), (1, 'd), (4, 'e)))
  * res0: List[Symbol] = List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)
  */
object DecodeRunLengthListP12 {
  def decode[T](list: List[Either[(Int, T), T]]): List[T] = {
    list.flatMap(t => repeatedElements(t))
  }

  def repeatedElements[T](t: Either[(Int, T), T]): Seq[T] = t match {
    case Left(t) => for (_ <- 1 to t._1) yield t._2
    case Right(value) => List(value)
  }
}
