package s99.list

/**
  * P08 (**) Eliminate consecutive duplicates of list elements.
  * If a list contains repeated elements they should be replaced with a single copy of the element. The order of the elements should not be changed.
  * Example:
  *
  * scala> compress(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
  * res0: List[Symbol] = List('a, 'b, 'c, 'a, 'd, 'e)
  */
object RemoveConsecutiveDup {
  def compress[T](list: List[T]): List[T] = list match {
    case Nil => List()
    case x::xs => compress(List(x), x, xs)
  }

  @scala.annotation.tailrec
  def compress[T](result: List[T], last: T, remain: List[T]): List[T] = remain match {
    case Nil => result
    case x::xs =>
      if (x == last) compress(result, x, xs)
      else compress(result :+ x, x, xs)
  }
}
