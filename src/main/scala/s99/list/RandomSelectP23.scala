package s99.list

import scala.collection.mutable
import scala.util.Random

object RandomSelectP23 {
  def randomSelect[T](n: Int, list: List[T]):List[T] = {
    if (n >= list.size) list
    else generateRandomNums(n, list.size).map(i => list(i)).toList
  }

  def randomSelectByRemove[T](n: Int, list: List[T]): List[T] = {
    if (n == 0) List()
    else {
      val r = Random.nextInt(list.size)
      val tuple = RemoveKthElementP20.removeAt(r, list)
      tuple._2::randomSelectByRemove(n - 1, tuple._1)
    }
  }

  def generateRandomNums(n: Int, max: Int): mutable.HashSet[Int] = {
    val set = new mutable.HashSet[Int]()
    while (set.size != n) {
      val r = Random.nextInt(max)
      if (!set.contains(r)) set.add(r)
    }
    set
  }
}
