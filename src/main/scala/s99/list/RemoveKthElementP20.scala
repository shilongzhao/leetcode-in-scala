package s99.list

object RemoveKthElementP20 {
  def removeAt[T](k: Int, list: List[T]): (List[T], T) = {
    (list.take(k) ++ list.drop(k + 1), list(k))
  }
}
