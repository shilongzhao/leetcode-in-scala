package s99.list

/**
  * P22 (*) Create a list containing all integers within a given range.
  * Example:
  * scala> range(4, 9)
  * res0: List[Int] = List(4, 5, 6, 7, 8, 9)
  */
object CreateRangeP22 {
  def range(start: Int, end:Int): List[Int] = {
    (for (i <- start to end) yield i).toList
  }

  def rangeR(start: Int, end: Int): List[Int] = {
    rangeRecursive(List(), start, end);
  }

  @scala.annotation.tailrec
  def rangeRecursive(result: List[Int], start: Int, end: Int): List[Int] = {
    if (start == end) result:+ end
    else rangeRecursive(result :+ start, start + 1, end)
  }
}
