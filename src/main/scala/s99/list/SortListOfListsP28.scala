package s99.list

object SortListOfListsP28 {
  def lsort[T](list: List[List[T]]): List[List[T]] = list.sortWith((l1, l2) => l1.size < l2.size)


  def lsortFreq[T](list: List[List[T]]): List[List[T]] = {
    val frequencies = scala.collection.mutable.Map[Int, Int]().withDefaultValue(0)
    list.foreach(l => frequencies.update(l.size, frequencies(l.size) + 1))
    list.sortWith((l1, l2) => frequencies(l1.size) < frequencies(l2.size))
  }
}
