package s99.list

/**
  * 5
  */
object ReverseList {
  def reverse[T](a: List[T]): List[T] = a match {
    case Nil => Nil
    case x :: xs => reverse(xs) :+ x
  }

  def reverseWithFold[T](a: List[T]): List[T] = {
    a.foldLeft(List[T]())((l, e) => e :: l)
  }
}
