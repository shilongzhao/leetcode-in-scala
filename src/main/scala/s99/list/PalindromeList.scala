package s99.list

/**
  * P06 (*) Find out whether a list is a palindrome.
  * Example:
  * scala> isPalindrome(List(1, 2, 3, 2, 1))
  * res0: Boolean = true
  */
object PalindromeList {
  def isPalindrome[T](list: List[T]): Boolean = {

    @scala.annotation.tailrec
    def palindrome(from:Int, to:Int): Boolean = {
      if (from >= to) true
      else if (list(from) == list(to)) palindrome(from + 1, to - 1)
      else false
    }

    palindrome(0, list.size - 1)
  }

  def isPalindromeReverse[T](a: List[T]): Boolean = a == a.reverse

  @scala.annotation.tailrec
  def isPalindrome3[T](a: List[T]): Boolean = a match {
    case Nil => true
    case List(_) => true
    case _ => a.head == a.last && isPalindrome3(a.tail.init)
  }

}
