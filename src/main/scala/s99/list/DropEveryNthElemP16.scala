package s99.list

object DropEveryNthElemP16 {
  def drop[T](n: Int, list: List[T]): List[T] = {
    if (n <= 0) list
    else {
      val seq = for (i <- 1 to list.size if i % n != 0) yield list(i - 1)
      seq.toList
    }
  }
}
