package s99.list

/**
  * 3
  */
object KthElementInList {
  def nth[T](n: Int, a: List[T]):Option[T] = {
    if (n == 0) a.headOption
    else if (a.isEmpty) None
    else nth(n - 1, a.tail)
  }
}
