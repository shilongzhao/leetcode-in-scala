package s99.list

object RotateN {
  def rotate[T](n: Int, list: List[T]): List[T] = {
    if (n >= 0) rotateLeft(n, list)
    else rotateLeft(list.size + n, list)
  }
  // if (n >= 0) list.drop(n) ::: list.take(n)
  @scala.annotation.tailrec
  def rotateLeft[T](n: Int, list: List[T]): List[T] =  {
    if (n == 0) list
    else rotateLeft(n - 1, list.tail :+ list.head)
  }

  def rotateByTakeDrop[T](n: Int, list: List[T]): List[T] = {
    if (n >= 0) list.drop(n):::list.take(n)
    else list.drop(list.size + n):::list.take(list.size + n)
  }
}
