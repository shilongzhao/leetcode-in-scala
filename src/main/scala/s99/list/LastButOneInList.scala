package s99.list

/**
  * 2
  */
object LastButOneInList {
  def penultimate[T](a: List[T]): Option[T] = {
    if (a.size <= 1) None
    else Some(a(a.size - 2))
  }
}
