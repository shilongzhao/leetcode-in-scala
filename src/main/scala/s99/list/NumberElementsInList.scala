package s99.list

/**
  * 4
  */
object NumberElementsInList {
 def length[T](a: List[T]): Int = a match {
  case Nil => 0
  case _::xs => 1 + length(xs)
 }

 def lengthWithFold[T](a: List[T]): Int = a.foldLeft(0)((l, _) => l + 1)

}
