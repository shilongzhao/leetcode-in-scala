package s99.list

/**
  * P07 (**) Flatten a nested list structure.
  * Example:
  * scala> flatten(List(List(1, 1), 2, List(3, List(5, 8))))
  * res0: List[Any] = List(1, 1, 2, 3, 5, 8)
  */
object FlattenNestedList {
  def flatten[T](elem: Element[T]): List[T] = elem match {
    case x: SE[T] => List(x.value)
    case xs: CE[T] => xs.elements.flatMap(ne => flatten(ne)).toList
  }


  def flatten2(list: List[Any]): List[Any] = list flatMap {
    case xs: List[_] => flatten2(xs)
    case x: Any => List(x)
  }

//  flatten2(List(1, List(2,3, List(4,5,6), List(7)), List(8,9)))
}

sealed trait Element[T]
case class SE[T](value: T) extends Element[T]
case class CE[T](elements: Element[T]*) extends Element[T]

