package s99.list

/**
  * 1
  */
object LastElementInList {
  @scala.annotation.tailrec
  def last[T](a: List[T]): Option[T] = a match {
    case Nil => None
    case e::Nil => Some(e)
    case _::t => last(t)
  }

  def main(args: Array[String]): Unit = {
    println(s"last number is ${ last(List(1,2,2,3,5,3)) }")

    println(s"last element is ${last(List())}")

    println(s"last element is ${last(Nil)}")

    println(s"last string is ${last(List("ab", "c", "exy"))}")
  }
}
