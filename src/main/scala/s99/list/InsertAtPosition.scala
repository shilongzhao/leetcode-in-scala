package s99.list

/**
  * P21 (*) Insert an element at a given position into a list.
  * Example:
  * scala> insertAt('new, 1, List('a, 'b, 'c, 'd))
  * res0: List[Symbol] = List('a, 'new, 'b, 'c, 'd)
  */
object InsertAtPosition {
  def insertAt[T](elem: T, n: Int, list: List[T]): List[T] = {
    (list.take(n) :+ elem) ++ list.drop(n)
  }
}
