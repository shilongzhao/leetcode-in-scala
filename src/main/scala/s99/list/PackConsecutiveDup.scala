package s99.list

/**
  * P09 (**) Pack consecutive duplicates of list elements into sublists.
  * If a list contains repeated elements they should be placed in separate sublists.
  * Example:
  *
  * scala> pack(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
  * res0: List[List[Symbol]] = List(List('a, 'a, 'a, 'a), List('b), List('c, 'c), List('a, 'a), List('d), List('e, 'e, 'e, 'e))
  */
object PackConsecutiveDup {
  def pack[T](list: List[T]): List[List[T]] = list match {
    case Nil => List()
    case x::xs => pack(List(), List(x), xs)
  }

  @scala.annotation.tailrec
  def pack[T](result: List[List[T]], current: List[T], remain: List[T]): List[List[T]] = remain match {
    case Nil => result :+ current;
    case x::xs =>
      if (x == current.head) pack(result, current :+ x, xs)
      else pack(result :+ current, List(x), xs)
  }
}
//def pack(list: List[Symbol]): List[List[Symbol]] = pack(List[List[Symbol]](), List[Symbol](), list)
//
//def pack(lists: List[List[Symbol]], list: List[Symbol], rem: List[Symbol]): List[List[Symbol]] = rem match {
//  case Nil => lists
//  case x::xs if (list.isEmpty || list.contains(x)) => pack(lists, list :+ x, xs)
//  case _ => pack(lists :+ list, List(), rem)
//}
