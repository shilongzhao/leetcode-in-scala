package s99.list

object SplitListP17 {
  def split[T](n: Int, list: List[T]): (List[T], List[T]) = list.splitAt(n)
//    (list.take(n), list.drop(n))
}
