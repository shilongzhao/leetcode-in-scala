package s99.list

object RandomPermutationP25 {
  def randomPermute[T](list: List[T]): List[T] = {
    RandomSelectP23.randomSelectByRemove(list.size, list)
  }
}
