package s99.list

object RepeatNElementsP15 {
  def duplicateN[T](n: Int, list: List[T]): List[T] = {
//    list.flatMap(e => (1 to n).map(_ => e).toList)
    list.flatMap(e => List.fill(n)(e))
  }
}
