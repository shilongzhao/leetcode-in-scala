package s99.list

object GroupElementsP27 {

  def combinations[T](n: Int, list: List[T]): List[List[T]] = list.combinations(n).toList

  def group[T](groups: List[Int], elements: List[T]): List[List[List[T]]] = groups match {
    // assert groups.sum == elements.size
    case Nil => Nil
    case _ =>
      val combos = combinations(groups.head, elements)
      combos.foldLeft(List[List[List[T]]]()) {
        (groupings, combo) => {
          val subGroupings = group(groups.tail, elements.diff(combo)) // recursion, all possible subgroupings without group.head and combo
          subGroupings match {
            case Nil => groupings ++ List(List(combo))
            case _ => groupings ++ subGroupings.map(combo::_) // add combo to the subgrouping, add subgroupings containing combo to final result
          }
        }
      }
  }



}
