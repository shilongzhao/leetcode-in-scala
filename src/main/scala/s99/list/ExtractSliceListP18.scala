package s99.list

object ExtractSliceListP18 {
  def slice[T](from: Int, to: Int, list: List[T]): List[T] = list.slice(from, to)
}
