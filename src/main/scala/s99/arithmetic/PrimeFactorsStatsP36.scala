package s99.arithmetic

/**
  * P36 (**) Determine the prime factors of a given positive integer (2).
  * Construct a list containing the prime factors and their multiplicity.
  * scala> 315.primeFactorMultiplicity
  * res0: List[(Int, Int)] = List((3,2), (5,1), (7,1))
  * Alternately, use a Map for the result.
  *
  * scala> 315.primeFactorMultiplicity
  *
  */
object PrimeFactorsStatsP36 {
  import PrimeFactorsP35._

  implicit def integerToPrimeFactorsMap(n: Int): PrimeFactorsMap = PrimeFactorsMap(n)

  case class PrimeFactorsMap(n: Int) {
    def primeFactorsMultiplicity: Map[Int, Int] =
      n.primeFactors.groupBy(identity).mapValues(_.size)

    def primeFactorsMultiplicity2: Map[Int, Int] =
      n.primeFactors.foldLeft(Map[Int, Int]() withDefaultValue 0)((m, i) => m.updated(i, m(i) + 1))
  }
}
