package s99.arithmetic

/**
  * P31 (**) Determine whether a given integer number is prime.
  * scala> 7.isPrime
  * res0: Boolean = true
  */
object IsPrime {
  def isPrime(n: Int): Boolean = {
    if (n < 2) false
    else !(2 to Math.sqrt(n).toInt).exists(n % _ == 0)
  }
}
