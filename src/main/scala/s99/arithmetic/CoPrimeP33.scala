package s99.arithmetic

/**
  * P33 (*) Determine whether two positive integer numbers are coprime.
  * Two numbers are coprime if their greatest common divisor equals 1.
  * scala> 35.isCoprimeTo(64)
  * res0: Boolean = true
  */
object CoPrimeP33 {
  implicit def fromIntToCoPrimeValidator(v: Int) = CoPrimeValidator(v)
  case class CoPrimeValidator(v: Int) {
    def isCoPrimeTo(another: Int): Boolean = 1 == GreatestCommonDivisorP32.gcd(v, another)
  }
}


