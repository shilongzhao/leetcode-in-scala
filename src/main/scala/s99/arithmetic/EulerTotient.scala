package s99.arithmetic

/**
  * P34 (**) Calculate Euler's totient function phi(m).
  * Euler's so-called totient function phi(m) is defined as the number of positive integers r (1 <= r <= m) that are coprime to m.
  * scala> 10.totient
  * res0: Int = 4
  *
  */
object EulerTotient {
  import CoPrimeP33._

  implicit def integerToEulerTotientCal(n: Int): EulerTotientCal = EulerTotientCal(n)

  case class EulerTotientCal(n: Int) {
    def totient: Int = (1 to n).count(_.isCoPrimeTo(n))
  }
}
