package s99.arithmetic

/**
  * P35 (**) Determine the prime factors of a given positive integer.
  * Construct a flat list containing the prime factors in ascending order.
  * scala> 315.primeFactors
  * res0: List[Int] = List(3, 3, 5, 7)
  */
object PrimeFactorsP35 {

  implicit def integerToPrimeFactor(n: Int): PrimeFactor = PrimeFactor(n)

  case class PrimeFactor(n: Int) {
    def primeFactors: List[Int] = primeFactors(2, n)

    def primeFactors(f: Int, n: Int): List[Int] = {
      if (n == 1) List()
      else if (n % f == 0) f :: primeFactors(f, n/f)
      else primeFactors(f + 1, n)
    }

  }
}
