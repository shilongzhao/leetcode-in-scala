package s99.arithmetic

/**
  * P40 (**) Goldbach's conjecture.
  * Goldbach's conjecture says that every positive even number greater than 2 is the sum of two prime numbers. E.g. 28 = 5 + 23. It is one of the most famous facts in number theory that has not been proved to be correct in the general case. It has been numerically confirmed up to very large numbers (much larger than Scala's Int can represent). Write a function to find the two prime numbers that sum up to a given even integer.
  * scala> 28.goldbach
  * res0: (Int, Int) = (5,23)
  */
object GoldbachConjectureP40 {

  implicit class GoldbachInt(value: Int) {
    def goldbach: (Int, Int) = {
      import PrimeNumbersInRangeP39._

      val primes = listPrimesInRange(2 to value)
      primes.find(p => primes.contains(value - p)).map(p => (p, value - p)).get
    }

    def goldbach2: (Int, Int) = {
      import IsPrime._
      (2 to value).find(p => isPrime(p) && isPrime(value - p)).map(p => (p, value - p)).get
    }
  }
}
