package string

object AddStringsAlter {
  def addStrings(num1: String, num2: String): String = {
    if (num1 == "0") num2
    else if (num2 == "0") num1
    else {
      val sb = new StringBuilder()
      var i = num1.length - 1
      var j = num2.length - 1
      var carry = 0
      while (i >= 0 || j >= 0 || carry > 0) {
        val x = if (i >= 0) num1.charAt(i) - '0' else 0
        val y = if (j >= 0) num2.charAt(j) - '0' else 0
        val sum = x + y + carry
        val d = sum % 10
        carry = sum / 10
        sb.append(d)
        i -= 1
        j -= 1
      }
      sb.reverse.toString()
    }
  }
}
