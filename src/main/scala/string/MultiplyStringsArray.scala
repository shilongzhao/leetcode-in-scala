package string

/**
  * author: zhaoshilong 
  * date: 2019-08-17
  */
object MultiplyStringsArray {

  // Runtime: 300 ms, faster than 87.10% of Scala online submissions for Multiply Strings.
  def multiply(num1: String, num2: String): String = {
    val array: Array[Int] = Array.ofDim(num1.length + num2.length)
    for (i <- num1.length - 1 to 0 by -1; j <- num2.length - 1 to 0 by -1) {
      val n = (num1.length + num2.length - 2) - (i + j)
      array(n) += num1.charAt(i).asDigit * num2.charAt(j).asDigit
    }

    // note the following operation cannot be put inside the previous for loop
    var carry = 0
    var result = ""
    for (i <- array.indices ) {
      val t = array(i) + carry
      carry = t / 10
      array(i) = t % 10
      result = array(i).toString ++ result
    }
    while (result.length > 1 && result.head == '0') result = result.tail

    result
  }

  def main(args: Array[String]): Unit = {
    println(multiply("345", "67"))

    println(multiply("0", "67"))

    println(multiply("9", "9"))

  }
}
