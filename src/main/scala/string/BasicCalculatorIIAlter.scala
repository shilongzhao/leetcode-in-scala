package string

object BasicCalculatorIIAlter {
  type Number = Int
  type Index = Int
  type Operator = Char

  def calculate(s: String): Int = {
    def drive(next: Index = 0, acc: Number = 0, op: Operator = '+'): Int = {
      val (num, end) = expand(next)
      if (end == s.length) operate(acc, op, num)
      else drive(end + 1, operate(acc, op, num), s(end))
    }

    def expand(i: Index, acc: Number = 1, op: Char = '*', next: Number = 0): (Number, Index) =
      if (i == s.length || s(i) == '+' || s(i) == '-') (operate(acc, op, next), i)
      else if (s(i) == '*' || s(i) == '/') expand(i + 1, operate(acc, op, next), s(i))
      else if (s(i).isDigit) expand(i + 1, acc, op, next * 10 + s(i).asDigit)
      else expand(i + 1, acc, op, next)

    def operate(x: Number, op: Char, y: Number): Int = op match {
      case '*' => x * y
      case '/' => x / y
      case '+' => x + y
      case '-' => x - y
    }

    drive()
  }

  def calculateX(s: String): Int = {
    var result = 0

    var prevNum = 0
    var prevOp = '+'
    var i = 0
    while (i < s.length) {
      if (s(i).isSpaceChar) {
        i += 1
      }
      else if (s(i).isDigit) {

        var num = 0

        while (i < s.length && s(i).isDigit) {
          num = num * 10 + s(i).asDigit
          i += 1
        }

        if (prevOp == '+') {
          result += prevNum
          prevNum = num
        }
        else if (prevOp == '-') {
          result += prevNum
          prevNum = -num
        }
        else if (prevOp == '*') {
          prevNum = prevNum * num
        }
        else if (prevOp == '/') {
          prevNum = prevNum / num
        }
      }
      else {
        prevOp = s(i)
        i += 1
      }
    }
    result += prevNum
    result
  }
}