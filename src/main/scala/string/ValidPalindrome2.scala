package string

object ValidPalindrome2 {

  /**
    * 680 https://leetcode.com/problems/valid-palindrome-ii/
    * @param s
    * @return
    */
  def validPalindrome(s: String): Boolean = {
    validPalindrome(s, 0, s.length - 1)
  }

  def validPalindrome(s: String, l: Int, r: Int): Boolean = {
    if (l >= r) true
    else if (s.charAt(l) == s.charAt(r)) validPalindrome(s, l + 1, r - 1);
    else isPalindrome(s, l + 1, r) || isPalindrome(s, l, r - 1)
  }

  def isPalindrome(s:String, l: Int, r: Int): Boolean = {
    if (l >= r) true
    else if (s.charAt(l) != s.charAt(r)) false
    else isPalindrome(s, l + 1, r - 1)
  }

  def main(args: Array[String]): Unit = {
//    println(validPalindrome("abc"))
//    println(validPalindrome("aba"))
//    println(validPalindrome("aa"))
//    println(validPalindrome("abba"))
//    println(validPalindrome("abca"))
    println(validPalindrome("eeccccbebaeeabebccceea"))
  }
}
