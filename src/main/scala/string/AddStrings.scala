package string

/**
  * https://leetcode.com/problems/add-strings/
  */
object AddStrings {
  // Runtime: 392 ms, faster than 29.17% of Scala online submissions for Add Strings.
  def addStrings(num1: String, num2: String): String = {
    val len = Math.max(num1.length, num2.length)
    val sum = Array.ofDim[Int](len + 1)

    val n1 = num1.reverse
    val n2 = num2.reverse
    var carry = 0
    for (i <- 0 to len) {
      sum(i) += carry
      if (i < n1.length) {
        sum(i) += n1(i).asDigit
      }
      if (i < n2.length) {
        sum(i) += n2(i).asDigit
      }
      val t = sum(i);
      carry = sum(i) / 10
      sum(i) = t % 10
    }

    val s = sum.reverse.dropWhile(_ == 0).foldLeft("")((s, x) => s + x.toString)
    if (s.isEmpty) "0" else s
  }

  def main(args: Array[String]): Unit = {
    println(addStrings("12343", "279"))
    println(addStrings("12345", ""))
    println(addStrings("98767", "99999"))
  }
}
