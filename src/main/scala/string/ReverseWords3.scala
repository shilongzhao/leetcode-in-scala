package string

/**
  * 557 https://leetcode.com/problems/reverse-words-in-a-string-iii/
  */
object ReverseWords3 {
  def reverseWords(s: String): String = {
    s.split(" ").map(s => s.reverse).mkString(" ")
  }
}
