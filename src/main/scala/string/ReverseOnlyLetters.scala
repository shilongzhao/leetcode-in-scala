package string

/**
  * 917 https://leetcode.com/problems/reverse-only-letters/
  */
object ReverseOnlyLetters {
  def reverseOnlyLetters(S: String): String = {
    def isalpha:Char => Boolean = c => ('a' to 'z') union ('A' to 'Z') contains c

    var j = S.length - 1
    var i = 0
    val chars = S.toCharArray
    while (i < j) {
      while (j > i && !isalpha(S(i))) i = i + 1
      while (j > i && !isalpha(S(j))) j = j - 1
      val c = chars(i)
      chars(i) = chars(j)
      chars(j) = c
      i += 1;
      j -= 1;
    }
    chars.mkString
  }
  def main(args: Array[String]): Unit = {
    var s = "Test1ng-Leet=code-Q!"
    print(reverseOnlyLetters(s))
  }
}
