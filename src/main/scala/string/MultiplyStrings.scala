package string

/**
  * author: zhaoshilong 
  * date: 2019-08-16
  *
  * 43 https://leetcode.com/problems/multiply-strings
  */
object MultiplyStrings {

  /**
    * Runtime: 800 ms, faster than 35.48% of Scala online submissions for Multiply Strings.
    *
    * @param num1 number input
    * @param num2 number input
    * @return
    */
  def multiply(num1: String, num2: String): String = {
    if (num1 == "0" || num2 == "0") "0" else {
      var s = ""
      var pf = "";
      var str2 = num2
      while (!str2.isEmpty) {
        val r = multiply(num1, str2.last) ++ pf
        s = sum(s, r)
        str2 = str2.init
        pf = pf ++ "0"
      }
      s
    }
  }


  def multiply(num1: String, factor: Char): String = {
    if (factor == '0') "0"
    else {
      val f = factor.asDigit
      var result = ""
      var str = num1
      var carry = 0
      while (!str.isEmpty) {
        val last = str.last.asDigit * f + carry
        result = (last % 10).toString ++ result
        carry = last / 10
        str = str.init
      }
      if (carry != 0) {
        carry.toString ++ result
      } else {
        result
      }
    }
  }

  def sum(num1: String, num2: String): String = {
    if (num1.isEmpty && num2.isEmpty) ""
    else {
      var str1 = num1
      var str2 = num2
      var result:String = ""
      var carry:Int = 0
      while (!str1.isEmpty || !str2.isEmpty) {
        val last = (if (str1.isEmpty) '0' else str1.last).asDigit + (if (str2.isEmpty) '0' else str2.last).asDigit + carry
        result = (last % 10).toString ++ result
        carry = last / 10
        str1 = if (str1.isEmpty) "" else str1.init
        str2 = if (str2.isEmpty) "" else str2.init
      }
      if (carry != 0) {
        carry.toString ++ result
      } else {
        result
      }
    }
  }

  def main(args: Array[String]): Unit = {
    println(sum("9123", "897"))

    println(multiply("123", '6'))

    println(multiply("123", "12"))
  }
}
