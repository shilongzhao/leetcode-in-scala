package string

/**
  * 1108 https://leetcode.com/problems/defanging-an-ip-address/
  */
object DefangeAddressIP {
  def defangIPaddr(address: String): String = {

    val builder = address.foldLeft(new StringBuilder())((s, c) => {
      if (c == '.') s.append("[.]") else s.append(c)
    })
    builder.toString
  }

  def main(args: Array[String]): Unit = {
    val ip = "1.2.3.4";
    print(defangIPaddr(ip))
  }
}
