package string

object BasicCalculatorII {
  //Runtime: 312 ms, faster than 76.92% of Scala online submissions for Basic Calculator II.
  def calculate(s: String): Int = {
    var result = 0
    var ops = List[Char]()
    var nums = List[Int]()

    var i = 0
    while (i < s.length) {
      if (s(i).isDigit) {
        var num = 0
        while (i < s.length && s(i).isDigit) {
          num = num * 10 + s(i).asDigit
          i += 1
        }

        if (ops.nonEmpty && (ops.head == '*' || ops.head == '/')) {
          num = calculate(ops.head, nums.head, num)
          nums = num :: nums.tail
          ops = ops.tail
        } else {
          nums = num :: nums
        }
      }
      else if (s(i) == '/' || s(i) == '*') {
        ops = s(i) :: ops
        i += 1
      }
      else if (s(i) == '+' || s(i) == '-') {
        if (ops.nonEmpty && nums.length >= 2) {
          val op = ops.head
          ops = ops.tail

          val num1 = nums.head;
          nums = nums.tail
          val num2 = nums.head
          nums = nums.tail

          val num = calculate(op, num2, num1)
          nums = num :: nums
        }
        ops = s(i) :: ops
        i += 1
      }
      else {
        i += 1
      }
    }

    if (ops.isEmpty) nums.head
    else calculate(ops.head, nums(1), nums.head)
  }

  def calculate(opt: Char, opr1: Int, opr2: Int) : Int = opt match {
    case '*' => opr1 * opr2
    case '+' => opr1 + opr2
    case '-' => opr1 - opr2
    case '/' => opr1 / opr2
  }
  def main(args: Array[String]): Unit = {
    println(calculate("2")) // 2
    println(calculate("13-22*2*3-14*2+3"))
    println(calculate("1-1+1"))
    println(calculate("1 + 2 * 3"))
    println(calculate(" 3 * 5 + 10 * 2"))
    println(calculate("12 * 2 + 3 * 4 * 5 / 6 + 22 * 11"))
  }
}
