package string

/**
  * 434 https://leetcode.com/problems/number-of-segments-in-a-string
  */
object NumberSegments {
  //  228 ms, faster than 42.86% of Scala online submissions for Number of Segments in a String.
  def countSegments(s: String): Int = {
    s.split("\\s+").count(_.nonEmpty)
  }

  def main(args: Array[String]): Unit = {
    println(countSegments(""))
    println(countSegments(" "))
    println(countSegments("\t"))
    println(countSegments("\t\t"))
    println(countSegments("a\t\tb\t"))
    println(countSegments("\ta\tb\t"))
  }
}
