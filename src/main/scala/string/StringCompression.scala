package string

/**
  * 443 https://leetcode.com/problems/string-compression
  */
object StringCompression {
  // 308 ms, faster than 50.00% of Scala online submissions for String Compression.
  def compress(chars: Array[Char]): Int = {
    var i = 0
    val builder = new StringBuilder()
    while (i < chars.length) {
      val c = chars(i)
      i += 1
      var count = 1
      while (i < chars.length && c == chars(i)) {
        count += 1
        i += 1
      }
      if (count == 1) builder.append(c) else builder.append(c).append(count)
    }
    val s = builder.toString()
    for (i <- s.indices) chars(i) = s(i)
    s.length
  }

  def main(args: Array[String]): Unit = {
    println(compress("".toCharArray));
    println(compress("bbbabbbbb".toCharArray))
    println(compress("abbcdddd".toCharArray))
  }

}
