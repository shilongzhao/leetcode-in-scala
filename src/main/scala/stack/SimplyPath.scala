package stack


/**
  * 71 https://leetcode.com/problems/simplify-path/
  */
object SimplyPath {
  // Runtime: 308 ms, faster than 80.00% of Scala online submissions for Simplify Path.
  def simplifyPath(path: String): String = {
    var stack = List[String]()
    val dirs = path.split("/")

    for (dir <- dirs) {
      if (dir == "..") {
        if (stack.nonEmpty) stack = stack.tail
      }
      else if (dir == "" || dir == ".") {}
      else stack = dir :: stack
    }
    if (stack.isEmpty) "/"
    else stack.reverse.foldLeft("")((p, d) => p + '/' + d)
  }

  def main(args: Array[String]): Unit = {
    println(simplifyPath(""))
    println(simplifyPath("/../"))
    println(simplifyPath("/home/"))
    println(simplifyPath("/home/szhao/Downloads/../Documents/"))
    println(simplifyPath("/home/szhao/Downloads//pdfs/"))

  }
}
