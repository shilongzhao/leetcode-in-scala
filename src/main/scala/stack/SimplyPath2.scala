package stack

object SimplyPath2 {
  def simplifyPath(path: String): String = {
    path.split("/")
      .foldLeft(List.empty[String]){
        case (_::tl,"..") => tl
        case (_, "..") => Nil
        case (lst, "."|"") => lst
        case (lst, dir) => dir :: lst
      }.reverse.mkString("/","/","")
  }
}
