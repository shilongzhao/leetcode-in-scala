package abs

/**
  * @author szhao
  */
class Concrete extends Abstract {
  override type T = String
  override def transform(x: String): String = x + x
  override val initial: String = "oui"
  override var current: String = initial
}
