package dp


/**
  * author: zhaoshilong 
  * date: 2019-08-18
  *
  * https://leetcode.com/problems/decode-ways/discuss/30357/DP-Solution-(Java)-for-reference/29441
  */
object DecodeWaysAlter {
  def numDecodings(s: String): Int = {
    val n = s.length
    val dp = Array.ofDim[Int](n + 1)

    dp(n) = 1
    for (i <- s.indices.reverse) {
      if (s(i) == '0') {
        dp(i) = 0
      }
      else {
        var sum = dp(i + 1)
        if (i < n - 1) {
          if (s(i) == '1' || (s(i) == '2' && s(i+1) >= '0' && s(i+1)<='6')) {
            sum += dp(i + 2)
          }
        }
        dp(i) = sum
      }
    }
    dp(0)
  }

}
