package dp

/**
  * author: zhaoshilong 
  * date: 2019-08-18
  *
  * https://leetcode.com/problems/decode-ways
  */
object DecodeWays {

  // Runtime: 240 ms, faster than 81.25% of Scala online submissions for Decode Ways.
  def numDecodings(s: String): Int = {
    if (s.isEmpty) 0
    else numDecodingsNonEmpty(s)
  }

  def numDecodingsNonEmpty(s: String): Int = {
    val n = s.length

    val dp = Array.ofDim[Int](n + 1)
    dp(0) = 1
    dp(1) = if (s.head.asDigit == 0) 0 else 1

    for (i <- 2 to n) {
      val d = s.charAt(i - 1).asDigit
      val d2 = s.substring(i - 2, i).toInt

      var sum = 0
      if (d >= 1 && d <= 9) sum += dp(i - 1)
      if (d2 >= 10 && d2 <= 26) sum += dp(i - 2)
      dp(i) = sum
    }

    dp(n)
  }

  def main(args: Array[String]): Unit = {
    println(numDecodings("120345"))
    println(numDecodings("00"))
    println(numDecodings("0"))
    println(numDecodings("30"))
    println(numDecodings("03"))
    println(numDecodings("12"))

  }
}
