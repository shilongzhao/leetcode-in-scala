// case matching - variable binding
case class Address(street: String, city: String, country: String)
case class Person(name: String, age: Int, address: Address)

val a = Person("Alex", 23, Address("Avenue de la Californie", "Nice", "France"))
val b = Person("Manon", 22, Address("Avenue Simone Veil", "Paris", "France"))
val c = Person("Carla", 21, Address("Corso Duca degli Abruzzi", "Torino", "Italy"))

for (person <- Seq(a, b, c)) {
  person match {
    case p @ Person("Alex", _, address) =>
      println(s"Hi Alex, $p")
    case p @ Person("Manon", 22, a @ Address(street, city, country)) =>
      println(s"Hi ${p.name}, age ${p.age}, city ${a.city}")
    case p @ Person(name, age, _) =>
      println(s"Hi there, $age old person $name, are you $p?")
  }
}




