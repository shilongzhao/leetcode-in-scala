package array

import scala.collection.mutable

object TwoSumAlter {
  def twoSum(nums: Array[Int], target: Int): Array[Int] = {
    val hashMap = mutable.HashMap.empty[Int, Int]
    for (i <- nums.indices) {
      if (hashMap.contains(target - nums(i))) {
        return Array(hashMap(target - nums(i)), i)
      }
      hashMap += (nums(i) -> i)

    }
    Array()
  }
}
