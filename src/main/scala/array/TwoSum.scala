package array

import scala.collection.Searching._
object TwoSum {
  def twoSum(nums: Array[Int], target: Int): Array[Int] = {
    val array = nums.zipWithIndex.sortWith((t1, t2) => t1._1 < t2._1)

    for (i <- 0 to array.length) {
      val index = binarySearch(array, target - array(i)._1)
      if (index >= 0 && index != array(i)._2) {
        return Array(array(i)._2, index)
      }
    }

    Array.ofDim[Int](0)
  }

  def binarySearch(arr: Array[(Int, Int)], key: Int): Int = {
    var left = 0
    var right = arr.length - 1
    while (right >= left) {
      val mid = left + (right - left)/2
      if (arr(mid)._1 == key) return arr(mid)._2
      else if (arr(mid)._1 > key) {
        right = mid - 1
      }
      else {
        left = mid + 1
      }
    }
    -1
  }
  def main(args: Array[String]): Unit = {
//    println(twoSum(Array(2,6,3,2,4), 10).mkString(","))
    println(twoSum(Array(3,2, 3), 6).mkString(","))
  }
}
