package array
import scala.math._

object Loops {
  def main(args: Array[String]): Unit = {
    for (i <- 1.to(10) if i % 2 == 0)
      println(i)

    for (i <- 1 to 10 if i % 2 == 0; j <- 1 to i if j % 2 == 1)
      println(s"$i $j")

    println(sqrt(2.2))

    println(decorate("zhao"))
    println(decorate(str = "zhao", left = "\\", right = "/"))

    val nums = new Array[Int](5)

    for (i <- nums.indices)
      nums(i) = i
    nums.foreach(println)

    val twiceNums = nums.map(i => i * 2)
    twiceNums.foreach(println)


    val s = Array("Hello", "World")
    s.foreach(println(_))
    s(0) = "Goodbye"
    for (elem <- s)
      println(elem)

    val u = s.map(s => s.toUpperCase())
    for (elem <- u)
      println(elem)


    // immutable map
    val scores = Map("Alice" -> 10, "Bob" -> 12, "Cindy" -> 20)
    val scoresSame = Map(("Alice", 10), ("Bob", 12), ("Cindy", 20))

    scores.foreach(println)

    println(scoresSame("Bob")) // 12

    val mutableScores = scala.collection.mutable.SortedMap("Bob" -> 10, "Bob" -> 12, "Alice" -> 20)
    mutableScores +=  ("Zelda" -> 11, "Mario" -> 18)
    mutableScores.foreach(println) // sorted
  }

  def decorate(str: String, left: String = "[", right: String = "]") = left + str + right


}
