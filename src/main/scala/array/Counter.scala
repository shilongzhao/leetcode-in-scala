package array

/**
  * author: zhaoshilong 
  * date: 2019-06-08
  */
class Counter {
  private var value = 0
  def increment() {value += 1}
  def current(): Int = value
}

object Counter {
  def main(args: Array[String]): Unit = {
    val myCounter = new Counter
    myCounter.increment
    println(myCounter.current)
  }
}
