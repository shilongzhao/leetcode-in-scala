package array

/**
  * author: zhaoshilong 
  * date: 2019-06-10
  * Enum
  */
object TrafficLightColor extends Enumeration {
  val Red, Yellow, Green = Value
}
