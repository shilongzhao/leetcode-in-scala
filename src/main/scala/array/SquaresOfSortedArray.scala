package array

/**
  * https://leetcode.com/problems/squares-of-a-sorted-array/
  */
object SquaresOfSortedArray {
  /**
    * Runtime: 7396 ms, faster than 45.76% of Scala online submissions for Squares of a Sorted Array.
    * Memory Usage: 58.8 MB, less than 85.71% of Scala online submissions for Squares of a Sorted Array.
    * @param A
    * @return
    */
  def sortedSquares(A: Array[Int]): Array[Int] = {
    A.map(a => a * a).sorted
  }

  def sortedSquaresLinearComplexity(array: Array[Int]): Array[Int] = {
    var i = 0
    var j = array.length - 1

    var result:Array[Int] = Array.ofDim(array.length)

    for (p <- array.length - 1 to 0 by -1) {
      if (math.abs(array(i)) > math.abs(array(j))) {
        result(p) = array(i) * array(i)
        i += 1
      }
      else {
        result(p) = array(j) * array(j)
        j -= 1
      }
    }
    result
  }

  def main(args: Array[String]): Unit = {
    var nums = Array(-4, -2, 0, 1, 3)
    sortedSquaresLinearComplexity(nums).foreach(println(_))
  }

}
