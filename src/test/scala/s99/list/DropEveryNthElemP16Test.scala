package s99.list

import org.scalatest.{FunSpec, Matchers}

class DropEveryNthElemP16Test extends FunSpec with Matchers {
  describe("P16 Spec") {
    it("should remove every third item in the list") {
      val result = DropEveryNthElemP16.drop(3, List("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k"))
      result should have length 8
      result should be(List("a", "b", "d", "e", "g", "h", "j", "k"))
    }

    it("should return same list when list has less items than n ") {
      val result = DropEveryNthElemP16.drop(3, List("a", "b"))
      result should have length 2
      result should be(List("a", "b"))
    }

    it("should return same list when n is 0") {
      val result = DropEveryNthElemP16.drop(0, List("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k"))
      result should have length 11
      result should be(List("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k"))
    }
  }
}

