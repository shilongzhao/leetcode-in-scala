package s99.list

import org.scalatest.{FunSpec, Matchers}

class RandomSelectP23Test extends FunSpec with Matchers {
  describe("P23 Spec") {
    it("should randomly select elements") {
      val randomElements: List[String] = RandomSelectP23.randomSelect(5, List("a", "b", "c", "d", "e", "f", "g", "h"))
      randomElements should have length 5
      println(randomElements)
      val set = randomElements.to[Set]
      set should have size 5
    }
  }

  describe("P23 Spec By remove from list") {
    it("should randomly select elements by removing from list") {
      val randomElements: List[String] = RandomSelectP23.randomSelectByRemove(5, List("a", "b", "c", "d", "e", "f", "g", "h"))
      randomElements should have length 5
      println(randomElements)
      val set = randomElements.to[Set]
      set should have size 5
    }
  }
}
