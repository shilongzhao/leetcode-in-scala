package s99.list

import org.scalatest.{FunSpec, Matchers}

class ModifiedRunLengthEncodeP11Test extends FunSpec with Matchers {

  describe("encode-modified spec") {
    it("should apply modified run-length on list") {
      val encodedList: List[Either[(Int, String), String]] =
        ModifiedRunLengthEncodeP11.encodeModified(List("a", "a", "a", "a", "b", "c", "c", "a", "a", "d", "e", "e", "e", "e"))
      encodedList should have size 6
      encodedList(0) should equal(Left(4, "a"))
      encodedList(1) should equal(Right("b"))
      encodedList(2) should equal(Left(2, "c"))
      encodedList(3) should equal(Left(2, "a"))
      encodedList(4) should equal(Right("d"))
      encodedList(5) should equal(Left(4, "e"))
    }
  }
}
