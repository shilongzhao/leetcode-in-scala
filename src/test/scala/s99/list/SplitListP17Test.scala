package s99.list

import org.scalatest.{FunSpec, Matchers}

class SplitListP17Test extends FunSpec with Matchers {
  describe("split list by n") {
    it("should split into two halves by length") {
      val (first, second) = SplitListP17.split(3, List("a", "b", "c", "d", "e", "f", "g", "h", "i", "k"))
      first should have length 3
      first should be(List("a", "b", "c"))
      second should have length 7
      second should be(List("d", "e", "f", "g", "h", "i", "k"))
    }

    it("should split into two halves by length with size 0 and list size when n is 0") {
      val (first, second) = SplitListP17.split(0, List("a", "b", "c", "d", "e", "f", "g", "h", "i", "k"))
      first should have length 0
      first should be(List())
      second should have length 10
      second should be(List("a", "b", "c", "d", "e", "f", "g", "h", "i", "k"))
    }
  }
}
