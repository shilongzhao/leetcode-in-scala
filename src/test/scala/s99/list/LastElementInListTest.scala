package s99.list

import org.scalatest.{FunSpec, Matchers}

class LastElementInListTest extends FunSpec with Matchers {
  describe("get last element in list a,b,c") {
    it("should return c") {
      val last = LastElementInList.last(List("a", "b", "c"))
      last should be(Some("c"))
    }
  }
}
