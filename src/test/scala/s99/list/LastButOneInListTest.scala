package s99.list

import org.scalatest.{FunSpec, Matchers}

class LastButOneInListTest extends FunSpec with Matchers {
  describe("get penultimate element in long list") {
    it("should return value") {
      val elem = LastButOneInList.penultimate(List(1, 2, 3, 4))
      elem should be(Some(3))
    }
  }
  describe("get penultimate element in one element list") {
    it("should return none") {
      val elem = LastButOneInList.penultimate(List(1))
      elem should be (None)
    }
  }

}
