package s99.list

import org.scalatest.{FunSpec, Matchers}

class NumberElementsInListTest extends FunSpec with Matchers {
  describe("length of list 1,2,3,4,5") {
    it("equals 5") {
      val l = NumberElementsInList.length(List(1,2,3,4,5))
      l should be(5)
    }
  }

  describe("length of empty list") {
    it("should be 0") {
      val l = NumberElementsInList.length(List())
      l should be(0)
    }
  }

  describe("fold length of list 1,2,3,4,5") {
    it("equals 5") {
      val l = NumberElementsInList.lengthWithFold(List(1,2,3,4,5))
      l should be(5)
    }
  }

  describe("fold length of empty list") {
    it("should be 0") {
      val l = NumberElementsInList.lengthWithFold(List())
      l should be(0)
    }
  }

}
