package s99.list

import org.scalatest.{FunSpec, Matchers}

class PalindromeListTest extends FunSpec with Matchers {
  describe("list a b b a") {
    it("is a palindrome") {
      val b = PalindromeList.isPalindrome(List("a", "b", "b", "a"))
      b should be(true)
    }
  }

  describe("list: a b c b a") {
    it("is a palindrome") {
      val b = PalindromeList.isPalindrome(List("a", "b", "c", "b", "a"))
      b should be(true)
    }
  }

  describe("empty list") {
    it("is a palindrome") {
      val b = PalindromeList.isPalindrome(List())
      b should be(true)
    }
  }

  describe("list: a b c a a") {
    it("is NOT a palindrome") {
      val b = PalindromeList.isPalindrome(List("a", "b", "c", "a", "a"))
      b should be(false)
    }
  }
}
