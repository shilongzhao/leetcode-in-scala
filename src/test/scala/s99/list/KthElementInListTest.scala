package s99.list

import org.scalatest.{FunSpec, Matchers}


class KthElementInListTest extends FunSpec with Matchers {
  describe("get k-th element in long list") {
    it("should be ok") {
      val k = KthElementInList.nth(3, List("a", "b", "c", "d", "e"));
      k should be (Some("d"))
    }
  }

  describe("get k-the element in short list") {
    it("should return none") {
      val k = KthElementInList.nth(10, List(1,2,3,4))
      k should be(None)
    }
  }
}
