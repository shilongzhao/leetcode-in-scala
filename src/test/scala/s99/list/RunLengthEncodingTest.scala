package s99.list

import org.scalatest.{FunSpec, Matchers}

class RunLengthEncodingTest extends FunSpec with Matchers {
  describe("list 1 1 2 3 3 3 4 4 4 5 5") {
    it("should return list (2,1) (1,2) (3,3) (3,4) (2,5)") {
      val r = RunLengthEncoding.encode(List(1, 1, 2, 3, 3, 3, 4, 4, 4, 5, 5))
      r should be(List((2,1), (1,2), (3,3), (3,4), (2,5)))
    }
  }
  describe("encode a list spec") {
    it("should encode consecutive duplicate elements") {
      val list = List("a", "a", "a", "a", "b", "c", "c", "a", "a", "d", "e", "e", "e", "e")
      val encodedList: List[(Int, String)] = RunLengthEncoding.encode(list)
      encodedList should have size 6
      encodedList.head should equal((4, "a"))
      encodedList(1) should equal((1, "b"))
      encodedList(2) should equal((2, "c"))
      encodedList(3) should equal((2, "a"))
      encodedList(4) should equal((1, "d"))
      encodedList(5) should equal((4, "e"))
    }
  }
}
