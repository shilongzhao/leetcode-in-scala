package s99.list

import org.scalatest.{FunSpec, Matchers}

class FlattenNestedListTest extends FunSpec with Matchers {
  describe("flatten [ [1,2,[3,4]], 5, [6,7] ]") {
    it("should be [1,2,3,4,5,6,7]") {
      val e = CE(
                CE(
                  SE(1),
                  SE(2),
                  CE(
                    SE(3),
                    SE(4)
                  )
                ),
                SE(5),
                CE(
                  SE(6),
                  SE(7)
                )
              );
      val r = FlattenNestedList.flatten(e)
      r should be(List(1,2,3,4,5,6,7))
    }
  }
}
