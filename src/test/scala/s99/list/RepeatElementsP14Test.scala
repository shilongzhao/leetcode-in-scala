package s99.list

import org.scalatest.{FunSpec, Matchers}

class RepeatElementsP14Test extends FunSpec with Matchers {
  describe("P14 Spec (recursion)") {
    it("should duplicate elements in a list") {
      val duplicateList: List[String] = RepeatElementsP14.duplicate(List("a", "b", "c", "d"))
      duplicateList should have length 8
      duplicateList should equal(List("a", "a", "b", "b", "c", "c", "d", "d"))
    }
  }

}
