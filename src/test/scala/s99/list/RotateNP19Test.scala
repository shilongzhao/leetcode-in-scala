package s99.list

import org.scalatest.{FunSpec, Matchers}

class RotateNP19Test extends FunSpec with Matchers {
  describe("P19 Spec") {
    it("should make head last element and second element from left as head") {
      val rotated = RotateN.rotate(1, List("a", "b", "c", "d", "e", "f", "g", "h"))
      rotated.head should equal("b")
      rotated.last should equal("a")
    }

    it("should return same list when n is 0") {
      val rotated = RotateN.rotate(0, List("a", "b", "c", "d", "e", "f", "g", "h"))
      rotated should be(List("a", "b", "c", "d", "e", "f", "g", "h"))
    }

    it("should rotate list by three elements") {
      val rotated = RotateN.rotate(3, List("a", "b", "c", "d", "e", "f", "g", "h"))
      rotated should be(List("d", "e", "f", "g", "h", "a", "b", "c"))
    }

    it("should rotate when n is negative") {
      val rotated = RotateN.rotate(-2, List("a", "b", "c", "d", "e", "f", "g", "h"))
      rotated should be(List("g", "h", "a", "b", "c", "d", "e", "f"))
    }
  }

  describe("P19 Rotate with drop/take") {
    it("should make head last element and second element from left as head") {
      val rotated = RotateN.rotateByTakeDrop(1, List("a", "b", "c", "d", "e", "f", "g", "h"))
      rotated.head should equal("b")
      rotated.last should equal("a")
    }

    it("should return same list when n is 0") {
      val rotated = RotateN.rotateByTakeDrop(0, List("a", "b", "c", "d", "e", "f", "g", "h"))
      rotated should be(List("a", "b", "c", "d", "e", "f", "g", "h"))
    }

    it("should rotate list by three elements") {
      val rotated = RotateN.rotate(3, List("a", "b", "c", "d", "e", "f", "g", "h"))
      rotated should be(List("d", "e", "f", "g", "h", "a", "b", "c"))
    }

    it("should rotate when n is negative") {
      val rotated = RotateN.rotateByTakeDrop(-2, List("a", "b", "c", "d", "e", "f", "g", "h"))
      rotated should be(List("g", "h", "a", "b", "c", "d", "e", "f"))
    }
  }

}
