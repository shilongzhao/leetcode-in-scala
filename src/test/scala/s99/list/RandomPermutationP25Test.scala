package s99.list

import org.scalatest.{FunSpec, Matchers}

class RandomPermutationP25Test extends FunSpec with Matchers{
  it("should generate random permutation of elements of a list") {
    val permutation: List[String] = RandomPermutationP25.randomPermute(List("a", "b", "c", "d", "e", "f"))
    permutation should have length 6
    permutation should contain theSameElementsAs List("a", "b", "c", "d", "e", "f")
  }
}
