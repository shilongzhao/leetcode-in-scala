package s99.list

import org.scalatest.{FunSpec, Matchers}

class DecodeRunLengthListP12Test extends FunSpec with Matchers {
  describe("decode spec") {
    it("should decode an run-length encoded list") {
      val decoded: List[String] = DecodeRunLengthListP12.decode(List(Left((4, "a")), Right("b"),
        Left((2, "c")), Left((2, "a")), Right("d"), Left((4, "e"))))
      decoded should have length 14
      decoded should equal(List("a", "a", "a", "a", "b", "c", "c", "a", "a", "d", "e", "e", "e", "e"))
    }
  }
}
