package s99.list

import org.scalatest.{FunSpec, Matchers}

class P18ExtractSliceListTest extends FunSpec with Matchers {

  describe("P18 Spec") {
    it("should return list between two indexes") {
      val result = ExtractSliceListP18.slice(3,7,List("a", "b", "c", "d", "e", "f", "g", "h", "i", "k"))
      result should have length 4
      result should be(List("d", "e", "f", "g"))
    }
  }
}
