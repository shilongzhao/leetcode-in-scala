package s99.list

import org.scalatest.{FunSpec, Matchers}

class RandomNumbersInRangeP24Test extends FunSpec with Matchers {
  describe("P24 Spec") {
    it("should give 6 random number between 1 and 49") {
      val randomList: List[Int] = RandomNumbersInRangeP24.lotto(6,  49)
      randomList should have length 6
      println(randomList)
    }
  }
}
