package s99.list

import org.scalatest.{FunSpec, Matchers}

class RemoveConsecutiveDupTest extends FunSpec with Matchers {
  describe("remove consecutive duplicates in list 1 2 2 3 4 4 4 5") {
    it("returns list 1 2 3 4 5") {
      val r = RemoveConsecutiveDup.compress(List(1,2,2,3,4,4,4,5))
      r should be(List(1,2,3,4,5))
    }
  }

  describe("remove consecutive duplicates in list 1 2 2 3 4 4 4 5 5") {
    it("returns list 1 2 3 4 5") {
      val r = RemoveConsecutiveDup.compress(List(1,2,2,3,4,4,4,5, 5))
      r should be(List(1,2,3,4,5))
    }
  }

  describe("remove consecutive duplicates in list 1 2 3 4 5") {
    it("returns list 1 2 3 4 5") {
      val r = RemoveConsecutiveDup.compress(List(1,2,3,4,5))
      r should be(List(1,2,3,4,5))
    }
  }

  describe("remove consecutive duplicates in list 1 1 1 1") {
    it("returns list 1") {
      val r = RemoveConsecutiveDup.compress(List(1,1,1,1))
      r should be(List(1))
    }
  }
}
