package s99.list

import org.scalatest.{FunSpec, Matchers}

class GenerateCombinationP26Test extends FunSpec with Matchers {
  describe("P26 generate combinations spec") {
    it("should generate all combinations of length 1") {
      val r = GenerateCombinationP26.combinations(1, List(1,2,3,4,5))
      r should have length 5
      r should contain theSameElementsAs List(List(1), List(2), List(3), List(4), List(5))
    }

    it("should generate all combinations of length 2") {
      val r = GenerateCombinationP26.combinations(2, List(1,2,3,4))
      r should have length 6
      r should contain theSameElementsAs List(List(1,2), List(1,3), List(1,4), List(2,3), List(2,4), List(3,4))
    }

    it("should generate all combinations of length 5") {
      val r = GenerateCombinationP26.combinations(5, List(1,2,3,4,5))
      r should have length 1
      r should contain theSameElementsAs List(List(1,2,3,4,5))
    }
  }
}
