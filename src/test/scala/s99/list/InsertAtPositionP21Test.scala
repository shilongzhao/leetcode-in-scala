package s99.list

import org.scalatest.{FunSpec, Matchers}

class InsertAtPositionP21Test extends FunSpec with Matchers {

  describe("P21 Spec") {
    it("should insert element at the second position") {
      val result = InsertAtPosition.insertAt("alfa", 1, List("a", "b", "c", "d"))
      result should have length 5
      result should be(List("a", "alfa", "b", "c", "d"))
    }


    it("should insert element at the zeroth position") {
      val result = InsertAtPosition.insertAt("alfa", 0, List("a", "b", "c", "d"))
      result should have length 5
      result should be(List("alfa", "a", "b", "c", "d"))
    }

    it("should insert element at the end position") {
      val result = InsertAtPosition.insertAt("alfa", 5, List("a", "b", "c", "d"))
      result should have length 5
      result should be(List("a", "b", "c", "d", "alfa"))
    }
  }
}
