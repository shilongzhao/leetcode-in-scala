package s99.list

import org.scalatest.{FunSpec, Matchers}

class RepeatNElementsP15Test extends FunSpec with Matchers {

  describe("P15 Spec") {
    it("should duplicate N times elements of a list") {
      val duplicates: List[String] = RepeatNElementsP15.duplicateN(3, List("a", "b", "c"))
      duplicates should have length 9
      duplicates should equal(List("a", "a", "a", "b", "b", "b", "c", "c", "c"))
    }
  }

}
