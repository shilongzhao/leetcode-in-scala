package s99.list

import org.scalatest.{FunSpec, Matchers}

class PackConsecutiveDupTest extends FunSpec with Matchers {

  describe("pack consecutive dup List(1,1,2,3,4,5,5,6)") {
    it("should return List(List(1,1), List(2), List(3), List(4), List(5,5), List(6))") {
      val r = PackConsecutiveDup.pack(List(1,1,2,3,4,5,5,6))
      r should be(List(List(1,1), List(2), List(3), List(4), List(5,5), List(6)))
    }
  }

}
