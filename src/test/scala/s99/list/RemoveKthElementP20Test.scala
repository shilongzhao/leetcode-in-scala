package s99.list

import org.scalatest.{FunSpec, Matchers}

class RemoveKthElementP20Test extends FunSpec with Matchers {
  describe("P20 spec") {
    it("should remove kth element from a list") {
      val result = RemoveKthElementP20.removeAt(1, List("a", "b", "c", "d"))
      result._2 should equal("b")
      result._1 should equal(List("a", "c", "d"))
    }

    it("should remove zeroth element from a list") {
      val result = RemoveKthElementP20.removeAt(0, List("a", "b", "c", "d"))
      result._1 should equal(List("b", "c", "d"))
      result._2 should equal("a")
    }
  }
}
