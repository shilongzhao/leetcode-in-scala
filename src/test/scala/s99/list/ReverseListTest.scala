package s99.list

import org.scalatest.{FunSpec, Matchers}

class ReverseListTest extends FunSpec with Matchers {
  describe("reverse an empty list") {
    it("returns an empty list") {
      val r = ReverseList.reverse(List())
      r should be(List())
    }
  }

  describe("reverse list 1,2,3,4,5") {
    it("returns list 5,4,3,2,1") {
      val r = ReverseList.reverse(List(1,2,3,4,5))
      r should be(List(5,4,3,2,1))
    }
  }

  describe("reverse list with fold 1,2,3,4,5") {
    it("returns list 5,4,3,2,1") {
      val r = ReverseList.reverseWithFold(List(1,2,3,4,5))
      r should be(List(5,4,3,2,1))
    }
  }
}
