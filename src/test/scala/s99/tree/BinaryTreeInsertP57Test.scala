package s99.tree

import org.scalatest.{FunSpec, Matchers}

class BinaryTreeInsertP57Test extends FunSpec with Matchers {
  describe("Spec 57 insert into BST") {
    it("should be able to insert integers") {
      End.addValue(1).toString should be("T(1 . .)")
    }

    it("should generate tree with expected shape") {
      Tree.fromList(List(3, 2, 5, 7, 1)).toString should be("T(3 T(2 T(1 . .) .) T(5 . T(7 . .)))")
    }

    it("should generate a symmetric tree from List(5, 3, 18, 1, 4, 12, 21)") {
      import SymmetricBinaryTreesP56._
      Tree.fromList(List(5, 3, 18, 1, 4, 12, 21)).isSymmetric should be(true)
    }

    it("should generate a symmetric tree from List(3, 2, 5, 7, 4)") {
      import SymmetricBinaryTreesP56._
      Tree.fromList(List(3, 2, 5, 7, 4)).isSymmetric should be(false)
    }

  }
}