package s99.tree

import org.scalatest.{FunSpec, Matchers}

/**
  * _____  _____  _____  _____  _____ 
  * |__   ||  |  ||  _  ||     ||   __|
  * |   __||     ||     ||  |  ||__   |
  * |_____||__|__||__|__||_____||_____|
  *
  */
class NodesAtLevelP62BTest extends FunSpec with Matchers{
  import s99.tree.NodesAtLevelP62B._
  describe("P62B spec") {
    it("should get all nodes in level") {
      val t = Tree('a', Tree('b'), Tree('c', Tree('d'), Tree('e')))
      t.atLevel(2) should contain theSameElementsAs List('b','c')
      t.atLevel(3) should contain theSameElementsAs List('d','e')
    }
  }

}
