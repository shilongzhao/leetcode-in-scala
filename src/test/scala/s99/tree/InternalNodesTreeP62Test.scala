package s99.tree

import org.scalatest.{FunSpec, Matchers}

/**
  * _____  _____  _____  _____  _____ 
  * |__   ||  |  ||  _  ||     ||   __|
  * |   __||     ||     ||  |  ||__   |
  * |_____||__|__||__|__||_____||_____|
  *
  */
class InternalNodesTreeP62Test extends FunSpec with Matchers{
  import s99.tree.InternalNodesTreeP62._
  describe("spec 62") {
    it ("should return list of internal nodes") {
      Tree(1, Tree(2), End).internalList should be(List(1))

      Tree(1, Tree(2, Tree(3), End), Tree(4, End, Tree(5))).internalList should contain theSameElementsAs List(1,2,4)
    }
  }

}
