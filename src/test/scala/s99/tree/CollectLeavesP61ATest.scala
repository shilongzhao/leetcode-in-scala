package s99.tree

import org.scalatest.{FunSpec, Matchers}

class CollectLeavesP61ATest extends FunSpec with Matchers {
  import CollectLeavesP61A._
  describe("Leaves List in Binary Tree P61A") {
    it ("should collect all leaf nodes") {
      Tree(1).leafList.map(n => n.value) should be(List(1))
    }

    it ("should collect all leaf nodes 2") {
      Tree(1, Tree(2), Tree(3)).leafList.map(n => n.value) should be(List(2,3))
    }

    it ("should collect all leaf nodes 3") {
      Tree('a', Tree('b'), Tree('c', Tree('d'), Tree('e'))).leafList.map(n => n.value) should be(List('b', 'd', 'e'))
    }
  }

}
