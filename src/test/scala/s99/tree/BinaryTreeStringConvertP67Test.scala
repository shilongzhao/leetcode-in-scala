package s99.tree

import org.scalatest.{FunSpec, Matchers}

class BinaryTreeStringConvertP67Test extends FunSpec with Matchers {
  import BinaryTreeStringConvertP67._
  describe("P67 binary tree to string") {
    it("should generate string") {
      val str = Tree('a', Tree('b', Tree('d'), Tree('e')), Tree('c', End, Tree('f', Tree('g'), End))).stringRep
      str should be("a(b(d,e),c(,f(g,)))")
    }
  }
}
