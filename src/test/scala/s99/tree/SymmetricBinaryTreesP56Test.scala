package s99.tree

import org.scalatest.{FunSpec, Matchers}

class SymmetricBinaryTreesP56Test extends FunSpec with Matchers {

  import s99.tree.SymmetricBinaryTreesP56._

  describe("Symmetric Binary Trees P56 Spec") {
    it("should be able to decide symmetric") {
      val root: Tree[Char] = Tree('a', Tree('b'), Tree('c'))
      root.isSymmetric should be(true)
    }

    it("should be able to decide asymmetric") {
      val root: Tree[Int] = Tree(1, Tree(2), Tree())
      root.isSymmetric should be(false)
    }

    it("should work with multiple nodes") {
      val left: Tree[Int] = Tree(3, Tree(4), End)
      val right: Tree[Int] = Tree(2, End, Tree(1))
      val root = Tree(0, left, right)
      root.isSymmetric should be(true)
    }

    it("should work with multiple nodes with false") {
      val left: Tree[Int] = Tree(3, Tree(4), End)
      val right: Tree[Int] = Tree(2, Tree(5), Tree(1))
      val root = Tree(0, left, right)
      root.isSymmetric should be(false)
    }
  }
}
