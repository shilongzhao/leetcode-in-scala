package s99.tree

import org.scalatest.{FunSpec, Matchers}

class CountLeavesP61Test extends FunSpec with Matchers {

  import CountLeavesP61._
  describe("Count Leaves P61 Spec") {
    it("should be able to count empty tree") {
      Tree().leafCount should be(0)
    }

    it("should be able to count 1-node tree") {
      Tree(1).leafCount should be(1)
    }

    it("should be able to count 2-nodes tree") {
      //2
      Tree(1, Tree(), Tree(2)).leafCount should be(1)
    }

    it("should be able to count 4-nodes tree") {
      //3,4
      Tree(1, Tree(2, Tree(3), End), Tree(4)).leafCount should be(2)

    }
    it("should be albe to count 7-nodes tree") {
      //3,4,6,7
      Tree(1, Tree(2, Tree(3), Tree(4)), Tree(5, Tree(6), Tree(7))).leafCount should be(4)
    }
  }
}
