package s99.logic

import org.scalatest.{FunSpec, Matchers}

class LogicTruthTableP46Test extends FunSpec with Matchers {

  describe("P46 Spec") {
    it("should generate truth table of a given logical expression (a and b)") {
      val tableContents = LogicTruthTableP46.table2(a => b => LogicTruthTableP46.and(a, b))
      tableContents should contain theSameElementsAs List((true, true, true), (true, false, false), (false, true, false), (false, false, false))
    }

    it("should generate truth table of a given logical expression (a xor b)") {
      val tableContents = LogicTruthTableP46.table2(a => b => LogicTruthTableP46.xor(a, b))
      tableContents should contain theSameElementsAs List((true, true, false), (true, false, true), (false, true, true), (false, false, false))
    }
  }
}
