package s99.multi_tree

import org.scalatest.{FunSpec, Matchers}

class ConstructTreeFromStringP70Test extends FunSpec with Matchers {
  import ConstructTreeFromStringP70._
  describe("P70 spec, construct caret string from multi-way tree") {
    it ("should be OK") {
      val caretString = MTree('a', List(MTree('f', List(MTree('g'))), MTree('c'), MTree('b', List(MTree('d'), MTree('e'))))).toCaretString
      caretString should be("afg^^c^bd^e^^^")
    }
  }
}
