package s99.multi_tree

import org.scalatest.{FunSpec, Matchers}

class CountNodesP70CTest extends FunSpec with Matchers {
  import CountNodesP70C._
  describe("P70C count nodes in multi-way tree") {
    it ("should be OK") {
      val n = MTree('a', List(MTree('f'))).nodeCount
      n should be(2)

      val r = MTree('a', List(MTree('f', List(MTree('g'))), MTree('c'), MTree('b', List(MTree('d'), MTree('e'))))).nodeCount
      r should be(7)
    }
  }

}
