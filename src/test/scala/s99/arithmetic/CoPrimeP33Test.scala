package s99.arithmetic

import org.scalatest.{FunSpec, Matchers}

class CoPrimeP33Test extends FunSpec with Matchers {
  import s99.arithmetic.CoPrimeP33._
  describe("P33 Spec") {
    it("33 49 should be co prime") {
      33.isCoPrimeTo(49) should be(true)
    }

    it("48, 72 should not be co prime") {
      72.isCoPrimeTo(48) should be(false)
    }
  }

}
