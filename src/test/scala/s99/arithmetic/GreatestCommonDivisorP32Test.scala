package s99.arithmetic

import org.scalatest.{FunSpec, Matchers}

class GreatestCommonDivisorP32Test extends FunSpec with Matchers {
  describe("P32 Spec GCD with Euclid's algorithm.") {
    it("15 is the gcd of 75, 30") {
      val r = GreatestCommonDivisorP32.gcd(30, 75)
      r should be(15)
    }

    it("13 should be the gcd of 39, 169") {
      val r = GreatestCommonDivisorP32.gcd(169, 39)
      r should be(13)
    }

    it ("1 should be the gcd of 7, 9") {
      val r = GreatestCommonDivisorP32.gcd(7, 9)
      r should be(1)
    }
  }

}
