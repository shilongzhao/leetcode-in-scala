package s99.arithmetic

import org.scalatest.{FunSpec, Matchers}

class EulersTotientImprovedP37Test extends FunSpec with Matchers {

  import s99.arithmetic.EulersTotientImprovedP37._
  describe("P37 Spec"){
    it("10 has 4 Euler totient"){
      val phi = 10.totientImproved
      phi should be(4)
    }

    it("9 has 6 Euler totient"){
      val phi = 9.totientImproved
      phi should be(6)
    }

    it("6 has 2 Euler totient"){
      val phi = 6.totientImproved
      phi should be(2)
    }
  }
}
