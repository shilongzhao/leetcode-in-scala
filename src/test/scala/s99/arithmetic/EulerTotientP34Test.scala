package s99.arithmetic

import org.scalatest.{FunSpec, Matchers}

class EulerTotientP34Test extends FunSpec with Matchers {
  import  EulerTotient._
  describe("P34 Euler's Totient") {
    it("10 totient should be 4") {
      10.totient should be(4)
    }

    it("11 totient should be 10") {
      11.totient should be(10)
    }

    it("9 totient should be 6") {
      9.totient should be(6)
    }
  }
}
