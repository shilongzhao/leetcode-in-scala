package s99.arithmetic

import org.scalatest.{FunSpec, Matchers}

class GoldbachConjectureP40Test extends FunSpec with Matchers {
  import s99.arithmetic.GoldbachConjectureP40._
  describe("P40 Spec") {
    it("8 is the sum of 5 and 3 prime numbers") {
      8.goldbach should be((3, 5))
      8.goldbach2 should be((3, 5))
    }

    it("4 is the sum of 2 and 2 prime numbers") {
      4.goldbach should be((2, 2))
      4.goldbach2 should be((2, 2))
    }

    it("28 is the sum of 5 and 23 prime numbers") {
      28.goldbach should be((5, 23))
      28.goldbach2 should be((5, 23))
    }
  }

}
