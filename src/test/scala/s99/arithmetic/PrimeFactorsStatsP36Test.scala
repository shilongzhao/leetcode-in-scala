package s99.arithmetic

import org.scalatest.{FunSpec, Matchers}

class PrimeFactorsStatsP36Test extends FunSpec with Matchers {
  import s99.arithmetic.PrimeFactorsStatsP36._
  describe("P36 Spec") {
    it("[[3,2],[5,1],[7,1]] is prime factors multiples of 315") {
      315.primeFactorsMultiplicity should be(Map(3->2, 5->1, 7->1))

      315.primeFactorsMultiplicity2 should be(Map(3->2, 5->1, 7->1))
    }

    it("[[3,1],[11,1]] is prime factors multiples of 33") {
      33.primeFactorsMultiplicity should be(Map(3->1, 11->1))

      33.primeFactorsMultiplicity2 should be(Map(3->1, 11->1))
    }

  }
}
