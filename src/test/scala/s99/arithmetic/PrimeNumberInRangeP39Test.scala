package s99.arithmetic

import org.scalatest.{FunSpec, Matchers}

class PrimeNumberInRangeP39Test extends FunSpec with Matchers {
  describe("P39 spec prime numbers in range") {
    it("prime numbers in range 7 to 31 should be [7,11,13,17,19,23,29,31]") {
      PrimeNumbersInRangeP39.listPrimesInRange(7 to 31) should be(List(7,11,13,17,19,23,29,31))
    }
  }
}
