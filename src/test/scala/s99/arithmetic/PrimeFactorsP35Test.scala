package s99.arithmetic

import org.scalatest.{FunSpec, Matchers}

class PrimeFactorsP35Test extends FunSpec with Matchers {
  import s99.arithmetic.PrimeFactorsP35._

  describe("Problem 35 Prime Factors Spec") {

    it("[2,2,2] are prime factors of 8") {
      val fs = 8.primeFactors
      fs should be(List(2, 2, 2))
    }

    it("[2,2,3] are prime factors of 12") {
      val fs = 12.primeFactors
      fs should be(List(2, 2, 3))
    }

    it("[3,3,5,7] are prime factors of 315") {
      val fs = 315.primeFactors
      fs should be(List(3, 3, 5, 7))
    }

    it("[3,11] are prime factors of 33") {
      val fs = 33.primeFactors
      fs should be(List(3, 11))
    }

    it("[11] are the prime factors of 11") {
      11.primeFactors should be(List(11))
    }
  }

}
