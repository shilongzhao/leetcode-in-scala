package s99.arithmetic

import org.scalatest.{FunSpec, Matchers}

class IsPrimeP31Test extends FunSpec with Matchers {
  describe("Problem 31 Spec") {
    it("neither 1 nor 0 is prime") {
      IsPrime.isPrime(1) should be(false)
      IsPrime.isPrime(0) should be(false)
    }

    it("7 is a prime number") {
      val prime = IsPrime.isPrime(7)
      prime should be(true)
    }

    it("10 is not prime number") {
      val prime = IsPrime.isPrime(10)
      prime should be(false)
    }

    it("15,485,863 is last prime below one million") {
      val prime = IsPrime.isPrime(15485863)
      prime should be(true)
    }

    it("86,028,121 is the last prime below five million") {
      val prime = IsPrime.isPrime(86028121)
      prime should be(true)
    }
  }
}
