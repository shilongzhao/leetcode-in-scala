package s99.arithmetic

import org.scalatest.{FunSpec, Matchers}

class GoldbachConjecturesP41Test extends FunSpec with Matchers {
  describe("P41 Spec") {
    it("should produce a list of goldbach compositions") {
      val goldaCompositions = GoldbachConjecturesP41.goldbachList(9 to 20)
      goldaCompositions should have length 6
      goldaCompositions should be(List((3, 7), (5, 7), (3, 11), (3, 13), (5, 13), (3, 17)))
    }

    it("should produce a list of goldbach compositions where both primes are greater than 50") {
      val goldaCompositions = GoldbachConjecturesP41.goldbachListLimited(2 to 2000, 50)
      goldaCompositions should have length 4
      goldaCompositions should be(List((73, 919), (61, 1321), (67, 1789), (61, 1867)))
    }
  }
}
